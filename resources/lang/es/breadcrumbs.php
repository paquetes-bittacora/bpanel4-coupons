<?php

declare(strict_types=1);

return [
    'bpanel4-coupons' => 'Gestión de cupones descuento',
    'index' => 'Listado',
    'create' => 'Nuevo',
    'edit' => 'Editar',
    'free-products' => 'Productos regalo',
];
