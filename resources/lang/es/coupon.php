<?php

declare(strict_types=1);

return [
    'code' => 'Código',
    'type' => 'Tipo',
    'discount_amount' => 'Cantidad de descuento',
    'discount_amount_help' => 'Si el tipo de cupón es "porcentaje", será el % de descuento aplicado al cupón, si es "Cantidad fija", será la cantidad de descuento aplicada',
    'min_cart_amount' => 'Importe mínimo del carrito',
    'min_cart_amount_help' => 'Si el carrito no alcanza este importe, no podrá aplicarse el cupón',
    'max_cart_amount' => 'Importe máximo del carrito',
    'max_cart_amount_help' => 'Si el carrito supera este importe, no podrá aplicarse el cupón',
    'disallow_if_discounted_products' => 'Deshabilitar si hay productos rebajados en el carrito',
    'user_usages_limit' => 'Límite de uso por usuario',
    'user_usages_limit_help' => 'Número de veces que un usuario podrá usar el cupón',
    'global_usages_limit' => 'Límite de uso global',
    'global_usages_limit_help' => 'Número de veces que podrá usarse el cupón en total (sumando los usos de todos los usuarios)',
    'name' => 'Nombre'
];
