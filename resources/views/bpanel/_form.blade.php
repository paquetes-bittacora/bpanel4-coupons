@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-coupons::form.new-product'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
            @if(isset($language))
                @include('language::partials.form-languages', ['model' => $coupon, 'edit_route_name' => 'bpanel4-coupons.edit',  'currentLanguage' => $language])
            @endif
        </div>

        <script>
          document.addEventListener('DOMContentLoaded', function () {
            const codeInput = document.getElementById('code');
            codeInput.addEventListener('keyup', function () {
              codeInput.value = codeInput.value.replace(' ', '_');
              codeInput.value = codeInput.value.toUpperCase();
            });
          });
        </script>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
            @csrf
            @livewire('form::select', ['name' => 'type', 'allValues' => $availableTypes, 'labelText' => __('bpanel4-coupons::coupon.type'), 'emptyValue' => false, 'selectedValues' => [$coupon?->getType()], 'fieldWidth' => 7])
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-coupons::coupon.name'), 'required'=>true, 'value' => old('name') ?? $coupon?->getName(), 'minlength' => 4 ])
            @livewire('form::input-text', ['name' => 'code', 'labelText' => __('bpanel4-coupons::coupon.code'), 'required'=>true, 'value' => old('code') ?? $coupon?->getCode(), 'minlength' => 4 ])
            @livewire('form::input-number', ['name' => 'discount_amount', 'labelText' => __('bpanel4-coupons::coupon.discount_amount'), 'required'=>true, 'value' => old('code') ?? $coupon?->getDiscountAmount(), 'min' => 0, 'step' => 0.01, 'fieldWidth' => 7, 'labelWidth' => 3 ])
            <div class="row mt-n3 mb-2">
                <div class="col-3"></div>
                <div class="col-9"><small class="text-xs">{{ __('bpanel4-coupons::coupon.discount_amount_help') }}</small></div>
            </div>
            @livewire('form::input-date', ['required' => false, 'labelText' => 'Fecha de caducidad', 'name' => 'expiration_date', 'idField' => 'expiration_date', 'enableTime' => true, 'fieldWidth' => 7, 'defaultDate' => false, 'value' => $coupon?->getExpirationDate()?->format('d/m/Y H:i')])
            @livewire('form::input-number', ['name' => 'min_cart_amount', 'labelText' => __('bpanel4-coupons::coupon.min_cart_amount'), 'required'=>false, 'value' => old('min_cart_amount') ?? $coupon?->getMinCartAmount(), 'min' => 0, 'step' => 0.01, 'fieldWidth' => 7, 'labelWidth' => 3 ])

            @livewire('form::input-number', ['name' => 'max_cart_amount', 'labelText' => __('bpanel4-coupons::coupon.max_cart_amount'), 'required'=>false, 'value' => old('max_cart_amount') ?? $coupon?->getMaxCartAmount(), 'min' => 0, 'step' => 0.01, 'fieldWidth' => 7, 'labelWidth' => 3 ])
            @livewire('form::input-number', ['name' => 'user_usages_limit', 'labelText' => __('bpanel4-coupons::coupon.user_usages_limit'), 'required'=> true, 'value' => old('user_usages_limit') ?? $coupon?->getUserUsagesLimit(), 'min' => 1, 'step' => 1, 'fieldWidth' => 7, 'labelWidth' => 3 ])
            <div class="row mt-n3 mb-2">
                <div class="col-3"></div>
                <div class="col-9"><small class="text-xs">{{ __('bpanel4-coupons::coupon.user_usages_limit_help') }}</small></div>
            </div>
            @livewire('form::input-number', ['name' => 'global_usages_limit', 'labelText' => __('bpanel4-coupons::coupon.global_usages_limit'), 'required'=> true, 'value' => old('global_usages_limit') ?? $coupon?->getglobalUsagesLimit(), 'min' => 1, 'step' => 1, 'fieldWidth' => 7, 'labelWidth' => 3 ])
            <div class="row mt-n3 mb-2">
                <div class="col-3"></div>
                <div class="col-9"><small class="text-xs">{{ __('bpanel4-coupons::coupon.global_usages_limit_help') }}</small></div>
            </div>
            @livewire('form::input-checkbox', [
                'name' => 'disallow_if_discounted_products',
                'value' => 1,
                'checked' => $coupon?->getDisallowIfDiscountedProducts() ?? false,
                'labelText' => __('bpanel4-coupons::coupon.disallow_if_discounted_products'), 'bpanelForm' => true
            ])
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @if($coupon?->getId() !== null)
                <input type="hidden" name="id" value="{{ $coupon->getId() }}">
                @method('put')
            @endif

            @if(isset($language))
                <input type="hidden" name="locale" value="{{ $language }}">
            @endif
        </form>
    </div>

    <script defer>
      {{-- Para los tipos de cupones "especiales" redirigimos a donde corresponda --}}
      document.getElementById('type').addEventListener('change', function (event) {
        if (event.target.value === 'free-products' && window.confirm('Se le redirigirá a una nueva página, ¿desea continuar?')) {
          window.location = '{{ route("bpanel4-coupons.free-products.create") }}';
        }
      });
    </script>
@endsection
