@if($row->type === 'free-products')
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-coupons.free-products', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el cupón?'], key('user-buttons-'.$row->id))
@else
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-coupons', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el cupón?'], key('user-buttons-'.$row->id))
@endif