@php
    /**
     * @var \Bittacora\Bpanel4\Coupons\Models\Coupon $row
     */
@endphp
<td>
    {{ $row->getName() }}
</td>
<td>
    {{ $row->getCode() }}
</td>
<td>
    {{ $row->getTypeName() }}
</td>
<td>
    {{ $row->getDiscountAmountWithUnit() }}
</td>
<td>
    {{ $row->getExpirationDate()?->format('d/m/Y H:i') }}
</td>
<td>
    {{ $row->getUserUsagesLimit() }}
</td>
<td>
    {{ $row->getGlobalUsagesLimit() }}
</td>
<td>
    {{ $row->created_at->format('d-m-Y H:i') }}
</td>
<td>
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-coupons', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el cupón?'], key('user-buttons-'.$row->id))
    </div>
</td>
