<div class="card bcard pb-4">
    <script>
      document.addEventListener('DOMContentLoaded', function () {
        const codeInput = document.getElementById('code');
        codeInput.addEventListener('keyup', function () {
          codeInput.value = codeInput.value.replace(' ', '_');
          codeInput.value = codeInput.value.toUpperCase();
        });
      });
    </script>
    @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-coupons::coupon.name'), 'required'=>true, 'value' => old('name') ?? $coupon?->getName(), 'minlength' => 1 ])
    @livewire('form::input-text', ['name' => 'code', 'labelText' => __('bpanel4-coupons::coupon.code'), 'required'=>true, 'value' => old('code') ?? $coupon?->getCode(), 'minlength' => 4 ])
    @livewire('form::input-date', ['required' => false, 'labelText' => 'Fecha de caducidad', 'name' => 'expiration_date', 'idField' => 'expiration_date', 'enableTime' => true, 'fieldWidth' => 7, 'defaultDate' => false, 'value' => $coupon?->getExpirationDate()?->format('d/m/Y H:i')])
    @livewire('form::input-number', ['name' => 'user_usages_limit', 'labelText' => __('bpanel4-coupons::coupon.user_usages_limit'), 'required'=> true, 'value' => old('user_usages_limit') ?? $coupon?->getUserUsagesLimit(), 'min' => 1, 'step' => 1, 'fieldWidth' => 7, 'labelWidth' => 3 ])
    <div class="row mt-n3 mb-2">
        <div class="col-3"></div>
        <div class="col-9"><small class="text-xs">{{ __('bpanel4-coupons::coupon.user_usages_limit_help') }}</small></div>
    </div>
    @livewire('form::input-number', ['name' => 'global_usages_limit', 'labelText' => __('bpanel4-coupons::coupon.global_usages_limit'), 'required'=> true, 'value' => old('global_usages_limit') ?? $coupon?->getglobalUsagesLimit(), 'min' => 1, 'step' => 1, 'fieldWidth' => 7, 'labelWidth' => 3 ])
    <div class="row mt-n3 mb-2">
        <div class="col-3"></div>
        <div class="col-9"><small class="text-xs">{{ __('bpanel4-coupons::coupon.global_usages_limit_help') }}</small></div>
    </div>
    <div class="form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label>Productos que deben estar en el carrito</label>
        </div>
        <div class="col-sm-7">
            <livewire:bpanel4-coupons::free-products.bpanel.livewire.products-selector
                    fieldName="required_products"
                    wire:key="required_products"
                    :products="$requiredProducts ?? []"
            />
        </div>
    </div>
    <div class="form-row mt-4">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label>Productos que se añadirán como regalo</label>
        </div>
        <div class="col-sm-7">
            <livewire:bpanel4-coupons::free-products.bpanel.livewire.products-selector
                    fieldName="gifted_products"
                    wire:key="required_products"
                    :products="$giftedProducts ?? []"
            />
        </div>
    </div>
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
    @if($coupon?->getId() !== null)
        <input type="hidden" name="id" value="{{ $coupon->getId() }}">
        @method('put')
    @endif

    @if(isset($language))
        <input type="hidden" name="locale" value="{{ $language }}">
    @endif
    @csrf
</div>