<div>
    @empty($products)
        <div class="alert alert-info"><i class="fas fa-exclamation-triangle"></i> Por favor, añada al menos un producto
        </div>
    @endempty
    @foreach($products as $index => $product)
        <div class="row">
            <div class="col-6">
                <livewire:form::select
                        :name="$fieldName . '[' . $index . '][id]'"
                        :allValues="$availableProducts"
                        labelText="Producto"
                        :emptyValue="false"
                        emptyValueText="Ninguno"
                        :multiple="false"
                        :selectedValues="isset($product['productId']) ? [$product['productId']] : null"
                        :wire:key="$fieldName . '_' . $index . '_product'"
                        :fieldWidth="10"
                        :labelWidth="2"
                />
            </div>
            <div class="col-5">
                <livewire:form::input-number
                        :name="$fieldName . '[' . $index . '][quantity]'"
                        min="1"
                        :required="true"
                        :fieldWidth="10"
                        :labelWidth="2"
                        labelText="Cantidad"
                        :wire:key="$fieldName . '_' . $index . '_quantity'"
                        :value="$product['quantity']"
                />
            </div>
            <div class="col-1">
                <button class="btn btn-danger" type="button"
                        wire:click="removeProduct({{ $index }})"
                        wire:key="remove-product-button-{{ $index }}">X
                </button>
            </div>
        </div>
    @endforeach
    <button class="btn btn-primary" wire:click="addProduct" type="button"><i class="fas fa-plus"></i> Añadir producto
    </button>
</div>