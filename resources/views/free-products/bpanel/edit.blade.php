@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Crear cupón con productos regalo')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">Editar cupón con productos regalo</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('bpanel4-coupons.free-products.update', ['coupon' => $coupon])}}">
            @include('bpanel4-coupons::free-products.bpanel._form', ['panelTitle' => __('bpanel4-coupons::form.edit-coupon')])
        </form>
    </div>

@endsection
