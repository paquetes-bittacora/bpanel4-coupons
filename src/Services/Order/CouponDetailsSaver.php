<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\Order;

use Bittacora\Bpanel4\Coupons\Actions\RegisterCouponUsage;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Coupons\Models\OrderCoupon;
use Bittacora\Bpanel4\Coupons\Services\CartCouponRemover;
use Bittacora\Bpanel4\Coupons\Services\CouponDiscountCalculator;
use Bittacora\Bpanel4\Orders\Models\Order\Order;
use Bittacora\Bpanel4\Clients\Services\ClientService;

final class CouponDetailsSaver
{
    public function __construct(
        private readonly ClientService $clientService,
        private readonly CouponDiscountCalculator $couponDiscountCalculator,
        private readonly CartCouponRemover $cartCouponRemover,
        private readonly RegisterCouponUsage $registerCouponUsage,
    ) {
    }

    public function saveCouponsDetails(Order $order): void
    {
        $cart = $this->clientService->getClientCart();

        $cart->getCoupons()->each(function (Coupon $coupon) use ($cart, $order): void {
            $orderCoupon = new OrderCoupon();
            $orderCoupon->order_id = $order->getId();
            $orderCoupon->coupon_id = $coupon->getId();
            $orderCoupon->code = $coupon->getCode();
            $orderCoupon->discount = $this->couponDiscountCalculator->calculateDiscountAsString($coupon, $cart);
            $this->cartCouponRemover->removeCouponFromCart($coupon);
            $orderCoupon->save();
            $this->registerCouponUsage->execute($this->clientService->getCurrentClient(), $coupon);
        });
    }
}
