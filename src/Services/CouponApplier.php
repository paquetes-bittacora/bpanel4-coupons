<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasReachedUsageLimitException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsAlreadyAppliedException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Str;

final class CouponApplier
{
    public function __construct(
        private readonly ClientService $clientService,
        private readonly CouponValidator $couponValidator,
        private readonly CartService $cartService,
    ) {
    }

    /**
     * @param string $code
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws UserNotLoggedInException
     * @throws CouponHasReachedUsageLimitException
     */
    public function apply(string $code): void
    {
        $coupon = Coupon::whereCode($code)->firstOrFail();
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);

        $this->checkIfCouponHadBeenAppliedBefore($cart, $code);

        $this->couponValidator->validateCoupon($coupon, $this->cartService);

        $cart->addCoupon($coupon);
        $this->applyCouponSpecificCartModifications($coupon, $cart);
    }

    private function checkIfCouponHadBeenAppliedBefore(Cart $cart, ?string $code): void
    {
        foreach ($cart->getCoupons() as $coupon) {
            if ($coupon->getCode() === $code) {
                throw new CouponIsAlreadyAppliedException();
            }
        }
    }

    private function getCouponApplierClassName(Coupon $coupon): string {
        return __NAMESPACE__ . '\\CouponAppliers\\' . ucfirst(Str::camel($coupon->getType())) . 'CouponApplier';
    }

    private function applyCouponSpecificCartModifications(Coupon $coupon, Cart $cart): void {
        $applierClassName = $this->getCouponApplierClassName($coupon);

        if (!class_exists($applierClassName)) {
            return;
        }

        $applier = resolve($applierClassName);

        if(!$applier instanceof CouponAppliers\CouponApplier) {
            return;
        }

        $applier->apply($cart, $coupon);
    }
}
