<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidations;

use Bittacora\Bpanel4\Coupons\Exceptions\ClientIsNotIncludedInCouponException;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;

final class ClientValidator
{
    /**
     * @throws ClientIsNotIncludedInCouponException
     */
    public function checkIfClientIsAllowed(CouponValidator $couponValidator): void
    {
        $allowedClients = $couponValidator->coupon->getAllowedClients()->pluck('id')
            ->map(static fn (string $item): int => (int)$item)->toArray();

        if ([] === $allowedClients) {
            return;
        }

        $cartClient = $couponValidator->cartService->getCart()->getClient();

        if (!in_array($cartClient->getClientId(), $allowedClients, true)) {
            throw new ClientIsNotIncludedInCouponException();
        }
    }
}
