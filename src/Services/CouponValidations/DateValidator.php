<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidations;

use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use DateTime;

final class DateValidator
{
    /**
     * @throws CouponHasExpiredException
     */
    public function checkExpirationDate(CouponValidator $couponValidator): void
    {
        if (null === $couponValidator->coupon->getExpirationDate()) {
            return;
        }
        if ($couponValidator->coupon->getExpirationDate() >= new DateTime()) {
            return;
        }
        throw new CouponHasExpiredException();
    }
}
