<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidations;

use Bittacora\Bpanel4\Coupons\Exceptions\CartDoesNotIncludeRequiredProductsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CartIncludesDisallowedProductsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;

final class CartProductValidator
{
    /**
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws CartDoesNotIncludeRequiredProductsException
     * @throws CartIncludesDisallowedProductsException
     */
    public function validate(CouponValidator $couponValidator): void
    {
        $this->checkIfAllowsDiscountedProducts($couponValidator);
        $this->checkIfCartContainsExcludedProducts($couponValidator);
        $this->checkIfCartContainsRequiredProducts($couponValidator);
    }

    /**
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     */
    private function checkIfAllowsDiscountedProducts(CouponValidator $couponValidator): void
    {
        if (!$couponValidator->cartService->hasDiscountedProducts()) {
            return;
        }
        if (!$couponValidator->coupon->getDisallowIfDiscountedProducts()) {
            return;
        }
        throw new CouponIsNotAvailableIfCartHasDiscountedProductsException();
    }

    /**
     * @throws CartIncludesDisallowedProductsException
     */
    private function checkIfCartContainsExcludedProducts(CouponValidator $couponValidator): void
    {
        $excludedProducts = $couponValidator->coupon->getExcludedProducts()->pluck('id')->toArray();

        foreach ($couponValidator->cartService->getCart()->getProducts() as $product) {
            if (in_array($product->getId(), $excludedProducts, true)) {
                throw new CartIncludesDisallowedProductsException();
            }
        }
    }

    /**
     * @throws CartDoesNotIncludeRequiredProductsException
     */
    private function checkIfCartContainsRequiredProducts(CouponValidator $couponValidator): void
    {
        /** @var array<int> $includedProducts */
        $includedProducts = $couponValidator->coupon->getRequiredProducts()->pluck('id')->toArray();

        if ([] === $includedProducts) {
            return;
        }

        if (!$this->cartContainsRequiredProducts($includedProducts, $couponValidator)) {
            throw new CartDoesNotIncludeRequiredProductsException();
        }
    }

    /**
     * @param array<int> $includedProducts
     */
    private function cartContainsRequiredProducts(array $includedProducts, CouponValidator $couponValidator): bool
    {
        foreach ($couponValidator->cartService->getCart()->getProducts() as $product) {
            if (in_array($product->getId(), $includedProducts, true)) {
                return true;
            }
        }
        return false;
    }
}
