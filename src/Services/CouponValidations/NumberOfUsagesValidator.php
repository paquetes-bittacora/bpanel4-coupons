<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidations;

use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasReachedUsageLimitException;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;

final class NumberOfUsagesValidator
{
    /**
     * @throws CouponHasReachedUsageLimitException
     */
    public function validate(CouponValidator $couponValidator): void
    {
        $this->checkIfGlobalUsageLimitHasBeenReached($couponValidator);
        $this->checkIfUserUsageLimitHasBeenReached($couponValidator);
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     */
    public function checkIfGlobalUsageLimitHasBeenReached(CouponValidator $couponValidator): void
    {
        if (null === $couponValidator->coupon->getGlobalUsagesLimit()) {
            return;
        }

        if ($couponValidator->coupon->usages()->count() < $couponValidator->coupon->getGlobalUsagesLimit()) {
            return;
        }

        throw new CouponHasReachedUsageLimitException();
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     */
    public function checkIfUserUsageLimitHasBeenReached(CouponValidator $couponValidator): void
    {
        $cartClient = $couponValidator->cartService->getCart()->getClient();
        if (!$couponValidator->coupon->getUserUsagesLimit()) {
            return;
        }
        if ($couponValidator->coupon->usages()->wherePivot(
            'client_id',
            $cartClient->getClientId()
        )->count() < $couponValidator->coupon->getUserUsagesLimit()) {
            return;
        }
        throw new CouponHasReachedUsageLimitException();
    }
}
