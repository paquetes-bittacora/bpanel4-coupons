<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidations;

use Bittacora\Bpanel4\Coupons\Exceptions\CartDoesNotIncludeRequiredCategoriesException;
use Bittacora\Bpanel4\Coupons\Exceptions\CartIncludesExcludedCategoriesException;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use Bittacora\Bpanel4\Products\Models\Product;

final class ProductCategoriesValidator
{
    /**
     * @throws CartDoesNotIncludeRequiredCategoriesException
     * @throws CartIncludesExcludedCategoriesException
     */
    public function validate(CouponValidator $couponValidator): void
    {
        $this->checkifCartContainsRequiredCategories($couponValidator);
        $this->checkIfCartContainsExcludedCategories($couponValidator);
    }

    /**
     * @throws CartIncludesExcludedCategoriesException
     */
    private function checkIfCartContainsExcludedCategories(CouponValidator $couponValidator): void
    {
        /** @var array<int> $excludedCategories */
        $excludedCategories = $couponValidator->coupon->getExcludedCategories()->pluck('id')
            ->map(static fn (string $item): int => (int)$item)->toArray();

        if ([] === $excludedCategories) {
            return;
        }

        if ($this->cartContainsCategories($excludedCategories, $couponValidator)) {
            throw new CartIncludesExcludedCategoriesException();
        }
    }

    /**
     * @param array<int> $categoriesToCheck
     */
    private function cartContainsCategories(array $categoriesToCheck, CouponValidator $couponValidator): bool
    {
        foreach ($couponValidator->cartService->getCart()->getProducts() as $product) {
            $product = Product::whereId($product->getId())->firstOrFail();
            $categories = $product->getCategories()->pluck('id')->toArray();
            if ([] !== array_intersect($categories, $categoriesToCheck)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @throws CartDoesNotIncludeRequiredCategoriesException
     */
    private function checkifCartContainsRequiredCategories(CouponValidator $couponValidator): void
    {
        /** @var array<int> $includedCategories */
        $includedCategories = $couponValidator->coupon->getRequiredCategories()->pluck('id')
            ->map(static fn (string $item): int => (int)$item)->toArray();

        if ([] === $includedCategories) {
            return;
        }

        if (!$this->cartContainsCategories($includedCategories, $couponValidator)) {
            throw new CartDoesNotIncludeRequiredCategoriesException();
        }
    }
}
