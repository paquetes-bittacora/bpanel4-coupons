<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidations;

use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;

final class CartAmountValidator
{
    /**
     * @throws CartAmountOutsideCouponLimitsException
     */
    public function checkCartAmount(CouponValidator $couponValidator): void
    {
        if ($this->cartAmountIsBelowMinimum($couponValidator) || $this->cartAmountIsAboveMaximum($couponValidator)) {
            throw new CartAmountOutsideCouponLimitsException();
        }
    }

    private function cartAmountIsBelowMinimum(CouponValidator $couponValidator): bool
    {
        if (null === $couponValidator->coupon->getMinCartAmount()) {
            return false;
        }
        return $couponValidator->cartTotal->toFloat() < $couponValidator->coupon->getMinCartAmount();
    }

    private function cartAmountIsAboveMaximum(CouponValidator $couponValidator): bool
    {
        if (null === $couponValidator->coupon->getMaxCartAmount()) {
            return false;
        }
        return $couponValidator->cartTotal->toFloat() > $couponValidator->coupon->getMaxCartAmount();
    }
}
