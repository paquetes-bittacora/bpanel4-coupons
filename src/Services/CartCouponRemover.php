<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Illuminate\Database\DatabaseManager;

final class CartCouponRemover
{
    public function __construct(
        private readonly ClientService $clientService,
        private readonly DatabaseManager $db,
    ) {
    }

    public function removeCouponFromCart(Coupon $coupon): void
    {
        $cart = $this->clientService->getClientCart();

        $this->db->delete(sprintf(
            'DELETE FROM coupons_carts WHERE coupon_id = %d AND cart_id = %d',
            $coupon->getId(),
            $cart->getId(),
        ));

        $this->deleteGiftedProducts($cart, $coupon);
    }

    private function deleteGiftedProducts(Cart $cart, Coupon $coupon): void
    {
        $this->db->delete(sprintf(
            'DELETE FROM cart_gifted_products WHERE coupon_id = %d AND cart_id = %d',
            $coupon->getId(),
            $cart->getId(),
        ));
    }
}
