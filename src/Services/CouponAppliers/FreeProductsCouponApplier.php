<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponAppliers;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Products\Models\CartProductFactory;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Illuminate\Support\Facades\App;

/**
 * Aplica un cupón del tipo productos regalo
 */
final class FreeProductsCouponApplier implements CouponApplier
{
    private Cart $cart;
    private Coupon $coupon;

    public function __construct(private readonly ClientService $clientService)
    {
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function apply(Cart $cart, Coupon $coupon): Cart
    {
        $this->coupon = $coupon;
        $this->cart = $cart;
        $coupon = $this->getCoupon();

        foreach ($coupon->getGiftedProducts() as $giftedProduct) {
            $cartProduct = $this->getCartableProduct($giftedProduct);
            $cart->addGiftedProduct($coupon, $cartProduct, $giftedProduct->pivot->quantity);
        }

        return $cart;
    }

    /**
     * Recibimos un cupón genérico pero necesitamos un FreeProductsCoupon
     */
    private function getCoupon(): FreeProductsCoupon
    {
        return FreeProductsCoupon::whereId($this->coupon->id)->firstOrFail();
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function getCartableProduct($giftedProduct): CartableProduct
    {
        $cartProductFactory = new CartProductFactory($this->clientService, App::make(ProductPresenterFactory::class));
        $cartProductFactory->setProduct($giftedProduct);
        return $cartProductFactory->make();
    }

}