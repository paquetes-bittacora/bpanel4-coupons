<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponAppliers;

use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;

interface CouponApplier
{
    public function apply(Cart $cart, Coupon $coupon): Cart;
}