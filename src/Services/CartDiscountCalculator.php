<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services;

use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Products\Models\CartProduct;
use Illuminate\Database\Eloquent\Collection;

final class CartDiscountCalculator
{
    private int $remainingProducts;
    private float $remainingDiscount;

    public function getDiscountByUnit(Cart $cart, CartService $cartService): array
    {
        $output = [];
        $this->remainingDiscount = $this->getRemainingDiscount($cart, $cartService);

        $products = $cart->getProducts();
        $products = $this->sortProductsByPrice($products);
        $this->remainingProducts = $cart->getItemCount();

        /** @var CartableProduct $product */
        foreach ($products as $product) {
            $totalProductAmount = $product->getUnitPrice()->toFloat() * $product->getQuantity();
            if ($totalProductAmount >= $this->getRemainingDiscountByUnit()){
                $output = $this->applyFullDiscountByUnit($product, $output);
                continue;
            }
            $output = $this->applyPartialDiscountByUnit($product, $output);
        }

        return $output;
    }

    public function getRemainingDiscount(Cart $cart, CartService $cartService): float
    {
        $totalDiscount = 0;
        $couponDiscountCalculator = app()->make(CouponDiscountCalculator::class, ['cartService' => $cartService]);
        foreach ($cart->getCoupons() as $coupon) {
            $totalDiscount += $couponDiscountCalculator->calculateDiscount($coupon, $cart)->toFloat();
        }

        return $totalDiscount;
    }

    /**
     * @param Collection $products
     * @return Collection
     */
    public function sortProductsByPrice(Collection $products
    ): Collection {
        $products = $products->sort(function (CartProduct $a, CartProduct $b): int {
            return ($a->getUnitPriceWithoutVat()->toInt() * $a->getQuantity()) <=>
                $b->getUnitPriceWithoutVat()->toInt() * $b->getQuantity();
        });
        return $products;
    }

    private function getRemainingDiscountByUnit(): float
    {
        if ($this->remainingProducts === 0) {
            return 0;
        }

        return $this->remainingDiscount / $this->remainingProducts;
    }

    /**
     * @param  array<int, float> $output
     * @return array<int, float>
     */
    private function applyPartialDiscountByUnit(CartableProduct $product, array $output): array
    {
        $output[$product->getProductId()] = $product->getUnitPrice()->toFloat();
        $this->remainingProducts -= $product->getQuantity();
        $this->remainingDiscount -= $product->getQuantity() * $product->getUnitPrice()->toFloat();
        return $output;
    }

    /**
     * @param  array<int, float> $output
     * @return array<int, float>
     */
    private function applyFullDiscountByUnit(CartableProduct $product, array $output): array
    {
        $remainingDiscountByUnit = $this->getRemainingDiscountByUnit();
        $output[$product->getProductId()] = $remainingDiscountByUnit;
        $this->remainingProducts -= $product->getQuantity();
        $this->remainingDiscount -= $remainingDiscountByUnit * $product->getQuantity();
        return $output;
    }
}
