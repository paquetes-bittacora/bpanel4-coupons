<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidators;

use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Services\CartService;

interface CouponValidator
{
    public function validate(Coupon $coupon, CartService $cartService): void;
}