<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services\CouponValidators;

use Bittacora\Bpanel4\Coupons\Exceptions\CartDoesNotIncludeRequiredProductsException;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;

final class FreeProductsCouponValidator implements CouponValidator
{
    private Cart $cart;
    private FreeProductsCoupon $coupon;

    /**
     * @throws CartDoesNotIncludeRequiredProductsException
     */
    public function validate(Coupon $coupon, CartService $cartService): void
    {
        $this->coupon = $this->getFreeProductsCoupon($coupon);
        $this->cart = $cartService->getCart();
        $this->checkCouponConditions();
    }

    private function getFreeProductsCoupon(Coupon $coupon): FreeProductsCoupon
    {
        return FreeProductsCoupon::whereId($coupon->id)->firstOrFail();
    }

    /**
     * @throws CartDoesNotIncludeRequiredProductsException
     */
    private function checkCouponConditions(): void
    {
        $remainingProducts = [];
        $requiredProducts = $this->coupon->getRequiredProducts();

        foreach ($requiredProducts as $requiredProduct) {
            $remainingProducts[$requiredProduct->id] = $requiredProduct->pivot->quantity;
        }

        foreach ($this->cart->getProducts() as $product) {
            if (!isset($remainingProducts[$product->getProductId()])) {
                continue;
            }
            $remainingProducts[$product->getProductId()] -= $product->getQuantity();
        }

        $remainingProducts = array_filter($remainingProducts, function ($remainingQuantity) {
            return $remainingQuantity > 0;
        });

        if (!empty($remainingProducts)) {
            throw new CartDoesNotIncludeRequiredProductsException(
                'El carrito no incluye los productos obligatorios para aplicar el cupón'
            );
        }
    }
}