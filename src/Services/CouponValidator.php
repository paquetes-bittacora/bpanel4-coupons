<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Coupons\Contracts\CouponUsageRepository;
use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasReachedUsageLimitException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Prices\Types\Price;
use DateTime;
use Str;
use function count;

final class CouponValidator
{
    private CartService $cartService;
    private Price $cartTotal;
    private Coupon $coupon;

    public function __construct(
        private readonly CouponUsageRepository $couponUsageRepository,
        private readonly ClientService $clientService,
    ) {
    }

    /**
     * @param Coupon $coupon
     * @param CartService $cartService
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     */
    public function validateCoupon(Coupon $coupon, CartService $cartService): void
    {
        $this->coupon = $coupon;
        $this->cartTotal = $cartService->getCartTotalWithoutTaxesBeforeDiscounts();
        $this->cartService = $cartService;
        $this->checkExpirationDate();
        $this->checkCartAmount();
        $this->checkIfAllowsDiscountedProducts();
        $this->checkUserUsageLimit();
        $this->checkGlobalUsageLimit();
        $this->applyCouponSpecificValidations($coupon, $cartService);
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     */
    private function checkCartAmount(): void
    {
        if ($this->cartAmountIsBelowMinimum() || $this->cartAmountIsAboveMaximum()) {
            throw new CartAmountOutsideCouponLimitsException();
        }
    }

    /**
     * @throws CouponHasExpiredException
     */
    private function checkExpirationDate(): void
    {
        if (null === $this->coupon->getExpirationDate()) {
            return;
        }
        if ($this->coupon->getExpirationDate() >= new DateTime()) {
            return;
        }
        throw new CouponHasExpiredException();
    }

    private function cartAmountIsBelowMinimum(): bool
    {
        if (null === $this->coupon->getMinCartAmount()) {
            return false;
        }
        return $this->cartTotal->toFloat() < $this->coupon->getMinCartAmount();
    }

    private function cartAmountIsAboveMaximum(): bool
    {
        if (null === $this->coupon->getMaxCartAmount()) {
            return false;
        }
        return $this->cartTotal->toFloat() > $this->coupon->getMaxCartAmount();
    }

    /**
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     */
    private function checkIfAllowsDiscountedProducts(): void
    {
        if (!$this->cartService->hasDiscountedProducts()) {
            return;
        }
        if (!$this->coupon->getDisallowIfDiscountedProducts()) {
            return;
        }
        throw new CouponIsNotAvailableIfCartHasDiscountedProductsException();
    }

    private function checkUserUsageLimit(): void
    {
        try {
            $client = $this->clientService->getCurrentClient();
            $userUsageLimit = $this->coupon->getUserUsageLimit();

            if ($userUsageLimit === null) {
                return;
            }

            if ($userUsageLimit <= count($this->couponUsageRepository->getCouponUsagesForClient($this->coupon,
                    $client))) {
                throw new CouponHasReachedUsageLimitException();
            }
        } catch (UserNotLoggedInException) {
            return;
        }
    }

    private function checkGlobalUsageLimit(): void
    {
        $globalUsageLimit = $this->coupon->getGlobalUsageLimit();

        if ($globalUsageLimit === null) {
            return;
        }

        if ($globalUsageLimit <= count($this->couponUsageRepository->getGlobalCouponUsages($this->coupon))) {
            throw new CouponHasReachedUsageLimitException();
        }
    }

    private function applyCouponSpecificValidations(Coupon $coupon, CartService $cartService): void {
        $applierClassName = $this->getCouponValidatorClassName($coupon);

        if (!class_exists($applierClassName)) {
            return;
        }

        $validator = resolve($applierClassName);

        if(!$validator instanceof CouponValidators\CouponValidator) {
            return;
        }

        $validator->validate($coupon, $cartService);
    }

    private function getCouponValidatorClassName(Coupon $coupon): string {
        return __NAMESPACE__ . '\\CouponValidators\\' . ucfirst(Str::camel($coupon->getType())) . 'CouponValidator';
    }
}
