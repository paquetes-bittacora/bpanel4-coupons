<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Services;

use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Exceptions\InvalidCouponTypeException;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Orders\Factories\CartServiceFactory;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\MonetaryAmount;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;

final class CouponDiscountCalculator
{
    private ?CartService $cartService = null;
    private Price $cartTotal;
    private Coupon $coupon;

    public function __construct(
        private readonly CartServiceFactory $cartServiceFactory,
        private readonly CouponValidator $couponValidator,
        ?CartService $cartService = null,
    ) {
        if (null !== $cartService) {
            $this->cartService = $cartService;
        }
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws InvalidPriceException
     * @throws ShippingOptionNotFoundException|InvalidCouponTypeException|CouponHasExpiredException
     * @throws CartAmountOutsideCouponLimitsException|CouponIsNotAvailableIfCartHasDiscountedProductsException
     */
    public function calculateDiscount(Coupon $coupon, Cart $cart): MonetaryAmount
    {
        $this->coupon = $coupon;
        $this->cartTotal = $this->getCartService($cart)->getCartTotalWithoutTaxesBeforeDiscounts();
        $this->couponValidator->validateCoupon($coupon, $this->getCartService($cart));

        $discountAmount = $this->getDiscountAmount();

        return new MonetaryAmount($discountAmount);
    }

    /**
     * @throws InvalidCouponTypeException
     * @throws ShippingOptionNotFoundException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws CouponHasExpiredException
     * @throws CartAmountOutsideCouponLimitsException
     * @throws InvalidPriceException
     */
    public function calculateDiscountAsString(Coupon $coupon, Cart $cart): string
    {
        $discount = $this->calculateDiscount($coupon, $cart);

        if ($discount->toFloat() > 0) {
            return '-' . $discount->toString();
        }

        return '';
    }

    private function getCartService(Cart $cart): CartService
    {
        if (null !== $this->cartService) {
            $this->cartService->setCart($cart);
            return $this->cartService;
        }

        return $this->cartServiceFactory->getCartService($cart);
    }

    /**
     * @throws InvalidCouponTypeException
     */
    private function getDiscountAmount(): float
    {
        if ('percentage' === $this->coupon->getType()) {
            return $this->coupon->getDiscountAmount() / 100 * $this->cartTotal->toFloat();
        }

        if ('fixed' === $this->coupon->getType()) {
            return $this->coupon->getDiscountAmount();
        }

        if ('free-products' === $this->coupon->getType()) {
            return 0;
        }

        throw new InvalidCouponTypeException();
    }
}
