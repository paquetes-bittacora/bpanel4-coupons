<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Dtos;

use Bittacora\Dtos\Dto;
use DateTime;

final class FreeProductsCouponDto extends Dto
{
    public function __construct(
        public readonly string $name,
        public readonly string $code,
        public readonly array $requiredProducts,
        public readonly array $giftedProducts,
        public readonly ?DateTime $expirationDate = null,
        public readonly ?int $userUsagesLimit = null,
        public readonly ?int $globalUsagesLimit = null,
    ) {
    }
}