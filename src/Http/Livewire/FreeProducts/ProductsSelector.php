<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Http\Livewire\FreeProducts;

use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\View\View;
use Livewire\Component;

final class ProductsSelector extends Component
{
    public array $products = [];
    public array $availableProducts = [];
    public array $selectedProducts = [];
    public string $fieldName;

    public function boot(): void
    {
        $this->availableProducts = Product::whereActive(true)
            ->orderBy('name')
            ->pluck('name', 'id')
            ->toArray()
        ;
    }

    public function render(): View
    {
        return view('bpanel4-coupons::free-products.bpanel.livewire.products-selector');
    }

    public function addProduct(): void
    {
        $this->products[] = ['productId' => null, 'quantity' => 0];
    }

    public function removeProduct(string $index): void
    {
        unset($this->products[$index]);
    }
}