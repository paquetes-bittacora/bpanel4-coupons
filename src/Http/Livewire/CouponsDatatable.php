<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Http\Livewire;

use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class CouponsDatatable extends DataTableComponent
{
    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name'),
            Column::make('Código', 'code'),
            Column::make('Tipo', 'type')->format(fn($value, Coupon $row, Column $column) => $row->getTypeName()),
            Column::make('Cantidad', 'discount_amount')->format(function($value, Coupon $row, Column $column) {
                if ($row->type === 'free-products') {
                    return '';
                }
                return $row->getDiscountAmountWithUnit();
            }),
            Column::make('Caducidad', 'expiration_date')->format(fn($value, Coupon $row, Column $column) => $row->getExpirationDate()?->format('d/m/Y H:i')),
            Column::make('Lím. uso/usuario', 'user_usages_limit')->format(fn($value, Coupon $row, Column $column) => $row->getUserUsagesLimit()),
            Column::make('Lím. uso/global', 'global_usages_limit')->format(fn($value, Coupon $row, Column $column) => $row->getGlobalUsagesLimit()),
            Column::make('Fecha de creación', 'created_at')->format(fn($value, Coupon $row, Column $column) => $row->created_at->format('d-m-Y H:i')),
            Column::make('Acciones', 'id')->view('bpanel4-coupons::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    /**
     * @return Builder<Coupon>
     */
    private function query(): Builder
    {
        return Coupon::query()
            ->orderBy('created_at', 'DESC')
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn ($query, $term) => $query
                ->where('name', 'like', '%' . $term . '%')
            );
    }

    private function rowView(): string
    {
        return 'bpanel4-coupons::bpanel.livewire.coupons-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            Coupon::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
