<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class EditCouponRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'numeric|required|exists:coupons',
            'name' => 'string|required',
            'code' => 'string|required|min:4',
            'type' => 'string|required',
            'discount_amount' => 'numeric|required|min:0',
            'expiration_date' => 'nullable',
            'min_cart_amount' => 'nullable|min:0|numeric',
            'max_cart_amount' => 'nullable|min:0|numeric|gt:min_cart_amount',
            'disallow_if_discounted_products' => 'nullable',
            'global_usages_limit' => 'nullable|min:0|numeric|gt:user_usages_limit',
            'user_usages_limit' => 'nullable|min:0|numeric',
        ];
    }
}
