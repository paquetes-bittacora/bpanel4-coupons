<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Coupons\Http\Requests\CreateCouponRequest;
use Bittacora\Bpanel4\Coupons\Http\Requests\EditCouponRequest;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use DateTime;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;

final class Bpanel4CouponsAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
        private readonly UrlGenerator $urlGenerator,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('bpanel4-coupons.index');
        return $this->view->make('bpanel4-coupons::bpanel.index');
    }

    public function create(): View
    {
        $this->authorize('bpanel4-coupons.create');
        return $this->view->make('bpanel4-coupons::bpanel.create', [
            'action' => '',
            'coupon' => null,
            'availableTypes' => (new Coupon())->getAvailableTypes(),
        ]);
    }

    public function store(CreateCouponRequest $request): RedirectResponse
    {
        try {
            Coupon::create($this->formatRequest($request->validated()));
            return $this->redirector->route('bpanel4-coupons.index')->with([
                'alert-success' => 'El cupón se creó correctamente',
            ]);
        } catch (Exception $exception) {
            report($exception);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al crear el cuppón',
            ]);
        }
    }

    public function edit(Coupon $coupon): View
    {
        $this->authorize('bpanel4-coupons.edit');
        return $this->view->make('bpanel4-coupons::bpanel.edit', [
            'action' => $this->urlGenerator->route('bpanel4-coupons.update', ['coupon' => $coupon]),
            'coupon' => $coupon,
            'availableTypes' => (new Coupon())->getAvailableTypes(),
        ]);
    }

    public function update(EditCouponRequest $request, Coupon $coupon): RedirectResponse
    {
        try {
            $coupon->update($this->formatRequest($request->validated()));
            return $this->redirector->route('bpanel4-coupons.index')->with([
                'alert-success' => 'El cupón se actualizó correctamente',
            ]);
        } catch (Exception $e) {
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al actualizar el cuppón',
            ]);
        }
    }

    public function destroy(Coupon $coupon){
        try {
            $coupon->delete();
            return $this->redirector->route('bpanel4-coupons.index')->with([
                'alert-success' => 'El cupón se eliminó correctamente',
            ]);
        } catch (Exception $e) {
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al eliminar el cuppón',
            ]);
        }
    }

    private function formatRequest(array $data): array
    {
        $data['code'] = strtoupper($data['code']);

        if (null === $data['expiration_date']) {
            return $data;
        }

        $data['expiration_date'] = DateTime::createFromFormat('d/m/Y H:i', $data['expiration_date']);

        return $data;
    }
}
