<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Coupons\Actions\FreeProducts\CreateFreeProductsCoupon;
use Bittacora\Bpanel4\Coupons\Actions\FreeProducts\UpdateFreeProductsCoupon;
use Bittacora\Bpanel4\Coupons\Dtos\FreeProductsCouponDto;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use DateTime;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Redirector;

final class Bpanel4CouponsFreeProductAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
        private readonly UrlGenerator $urlGenerator,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('bpanel4-coupons.create');
        return $this->view->make('bpanel4-coupons::free-products.bpanel.create', [
            'coupon' => null,
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request, CreateFreeProductsCoupon $createFreeProductsCoupon): RedirectResponse
    {
        $this->authorize('bpanel4-coupons.store');
        $data = $request->all();
        $data['expiration_date'] = !empty($data['expiration_date']) ?
            DateTime::createFromFormat('d/m/Y H:i', $data['expiration_date']) :
            null
        ;
        $dto = FreeProductsCouponDto::fromArray($data);
        $coupon = $createFreeProductsCoupon->execute($dto);

        if ($request->has('save')) {
            return $this->redirector->to($this->urlGenerator->route('bpanel4-coupons.free-products.index'));
        } else {
            return $this->redirector->to($this->urlGenerator->route('bpanel4-coupons.free-products.edit', [
                'coupon' => $coupon
            ]));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(FreeProductsCoupon $coupon): View
    {
        $this->authorize('bpanel4-coupons.edit');
        $requiredProducts = $this->getFormattedProducts($coupon->getRequiredProducts());
        $giftedProducts = $this->getFormattedProducts($coupon->getGiftedProducts());
        return $this->view->make('bpanel4-coupons::free-products.bpanel.edit', [
            'coupon' => $coupon,
            'requiredProducts' => $requiredProducts,
            'giftedProducts' => $giftedProducts,
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(
        Request $request,
        FreeProductsCoupon $coupon,
        UpdateFreeProductsCoupon $updateFreeProductsCoupon
    ): RedirectResponse {
        $this->authorize('bpanel4-coupons.update');
        $data = $request->all();
        $data['expiration_date'] = !empty($data['expiration_date']) ?
            DateTime::createFromFormat('d/m/Y H:i', $data['expiration_date']) :
            null
        ;
        $updateFreeProductsCoupon->execute($coupon, FreeProductsCouponDto::fromArray($data));

        if ($request->has('save')) {
            return $this->redirector
                ->to($this->urlGenerator
                ->route('bpanel4-coupons.free-products.index'))
                ->with('alert-success', __('Cupón actualizado'))
                ;
        }

        return $this->redirector
            ->to($this->urlGenerator
            ->route('bpanel4-coupons.free-products.edit', ['coupon' => $coupon]))
            ->with('alert-success', __('Cupón actualizado'))
            ;
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(FreeProductsCoupon $coupon): RedirectResponse
    {
        $this->authorize('bpanel4-coupons.destroy');
        $coupon->products()->detach();
        $coupon->giftedProducts()->detach();
        $coupon->delete();

        return $this->redirector
            ->to($this->urlGenerator
                ->route('bpanel4-coupons.index'))
            ->with('alert-success', __('Cupón eliminado'))
            ;
    }

    private function getFormattedProducts(Collection $productsCollection): array
    {
        $products = [];
        foreach ($productsCollection as $product) {
            $products[] = [
                'productId' => $product->id,
                'quantity' => $product->pivot->quantity,
            ];
        }
        return $products;
    }
}
