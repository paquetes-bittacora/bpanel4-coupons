<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Http\Controllers;

use Bittacora\Bpanel4\Coupons\Exceptions\NotApplicableCouponException;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Coupons\Services\CartCouponRemover;
use Bittacora\Bpanel4\Coupons\Services\CouponApplier;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

final class Bpanel4CouponsPublicController
{
    public function __construct(
        private readonly Redirector $redirector,
    ) {
    }

    public function apply(Request $request, CouponApplier $couponApplier): RedirectResponse
    {
        try {
            if (empty($request->get('code'))) {
                return $this->redirector->back()->with([
                    'alert-danger' => 'No se introdujo ningún código de descuento',
                ]);
            }
            $code = strtoupper(trim($request->get('code')));
            $couponApplier->apply($code);
            return $this->redirector->back()->with(['alert-success' => 'Cupón aplicado']);
        } catch (NotApplicableCouponException $e){
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al aplicar el cupón: ' . $e->getMessage(),
            ]);
        } catch (Exception $e) {
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al aplicar el cupón. Compruebe si el código es correcto',
            ]);
        }
    }

    public function removeCouponFromCart(Coupon $coupon, CartCouponRemover $cartCouponRemover): RedirectResponse
    {
        $cartCouponRemover->removeCouponFromCart($coupon);
        return $this->redirector->back()->with(['alert-success' => 'Cupón eliminado del carrito']);
    }
}
