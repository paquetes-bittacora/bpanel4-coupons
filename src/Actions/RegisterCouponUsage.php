<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Actions;

use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Clients\Models\Client;

final class RegisterCouponUsage
{
    public function execute(Client $client, Coupon $coupon): void
    {
        $coupon->registerUsage($client);
    }
}
