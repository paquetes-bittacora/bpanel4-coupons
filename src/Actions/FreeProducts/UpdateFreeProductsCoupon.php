<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Actions\FreeProducts;

use Bittacora\Bpanel4\Coupons\Dtos\FreeProductsCouponDto;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;

final class UpdateFreeProductsCoupon
{
    public function execute(FreeProductsCoupon $coupon, FreeProductsCouponDto $dto): FreeProductsCoupon
    {
        $coupon->name = $dto->name;
        $coupon->code = $dto->code;
        $coupon->discount_amount = 0;
        $coupon->expiration_date = $dto->expirationDate;
        $coupon->user_usages_limit = $dto->userUsagesLimit;
        $coupon->global_usages_limit = $dto->globalUsagesLimit;
        $coupon->save();
        $this->updateRequiredProducts($coupon, $dto);
        $this->updateGiftedProducts($coupon, $dto);

        $coupon->refresh();
        return $coupon;
    }

    private function updateRequiredProducts(FreeProductsCoupon $coupon, FreeProductsCouponDto $dto): void
    {
        $coupon->products()->detach();
        foreach ($dto->requiredProducts as $requiredProduct) {
            $coupon->products()->attach($requiredProduct['id'], [
                'quantity' => $requiredProduct['quantity'],
                'action' => 'include'
            ]);
        }
    }

    private function updateGiftedProducts(FreeProductsCoupon $coupon, FreeProductsCouponDto $dto): void
    {
        $coupon->giftedProducts()->detach();
        foreach ($dto->giftedProducts as $giftedProduct) {
            $coupon->giftedProducts()->attach($giftedProduct['id'], [
                'quantity' => $giftedProduct['quantity'],
            ]);
        }
    }
}