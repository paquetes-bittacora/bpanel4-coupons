<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Actions\FreeProducts;

use Bittacora\Bpanel4\Coupons\Dtos\FreeProductsCouponDto;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;

final class CreateFreeProductsCoupon
{
    public function execute(FreeProductsCouponDto $dto): FreeProductsCoupon
    {
        $coupon = new FreeProductsCoupon();
        $coupon->type = 'free-products';
        $coupon->name = $dto->name;
        $coupon->code = $dto->code;
        $coupon->discount_amount = 0;
        $coupon->expiration_date = $dto->expirationDate;
        $coupon->user_usages_limit = $dto->userUsagesLimit;
        $coupon->global_usages_limit = $dto->globalUsagesLimit;
        $coupon->save();
        $this->addRequiredProducts($coupon, $dto);
        $this->addGiftedProducts($coupon, $dto);

        return $coupon;
    }

    private function addRequiredProducts(FreeProductsCoupon $coupon, FreeProductsCouponDto $dto): void
    {
        foreach ($dto->requiredProducts as $requiredProduct) {
            $coupon->products()->attach($requiredProduct['id'], [
                'quantity' => $requiredProduct['quantity'],
                'action' => 'include'
            ]);
        }
    }

    private function addGiftedProducts(FreeProductsCoupon $coupon, FreeProductsCouponDto $dto): void
    {
        foreach ($dto->giftedProducts as $giftedProduct) {
            $coupon->giftedProducts()->attach($giftedProduct['id'], [
                'quantity' => $giftedProduct['quantity'],
            ]);
        }
    }
}