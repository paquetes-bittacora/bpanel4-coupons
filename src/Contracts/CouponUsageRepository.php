<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Contracts;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Coupons\Models\Coupon;

interface CouponUsageRepository
{
    public function getCouponUsagesForClient(Coupon $coupon, Client $client);

    public function getGlobalCouponUsages(Coupon $coupon);
}
