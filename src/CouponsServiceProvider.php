<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons;

use Bittacora\Bpanel4\Coupons\Commands\InstallCommand;
use Bittacora\Bpanel4\Coupons\Contracts\CouponUsageRepository;
use Bittacora\Bpanel4\Coupons\Http\Livewire\CouponsDatatable;
use Bittacora\Bpanel4\Coupons\Http\Livewire\FreeProducts\ProductsSelector;
use Bittacora\Bpanel4\Coupons\Listeners\DisplayAdditionalTotalRowsHook;
use Bittacora\Bpanel4\Coupons\Repositories\EloquentCouponUsageRepository;
use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Illuminate\Support\ServiceProvider;
use Livewire\LivewireManager;

final class CouponsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-coupons';

    public function boot(
        Hooks $hooks,
        LivewireManager $livewire
    ): void {
        $this->commands(InstallCommand::class);
        $this->registerHookCallbacks($hooks);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $livewire->component(self::PACKAGE_PREFIX . '::livewire.coupons-table', CouponsDatatable::class);
        $livewire->component(
            self::PACKAGE_PREFIX . '::free-products.bpanel.livewire.products-selector',
            ProductsSelector::class
        );
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'coupons-admin');

        $this->app->bind(CouponUsageRepository::class, EloquentCouponUsageRepository::class);
    }

    private function registerHookCallbacks(Hooks $hooks): void
    {
        $hooks->register('display-additional-total-rows', [
            $this->app->make(DisplayAdditionalTotalRowsHook::class),
            'handle',
        ]);
    }
}
