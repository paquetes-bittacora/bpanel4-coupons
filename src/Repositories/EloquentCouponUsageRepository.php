<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Repositories;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Coupons\Contracts\CouponUsageRepository;
use Bittacora\Bpanel4\Coupons\Models\Coupon;

final class EloquentCouponUsageRepository implements CouponUsageRepository
{
    public function getCouponUsagesForClient(Coupon $coupon, Client $client): array
    {
        return $coupon->usages()->where('client_id', $client->getClientId())->get()->toArray();
    }

    public function getGlobalCouponUsages(Coupon $coupon): array
    {
        return $coupon->usages()->get()->toArray();
    }
}