<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Listeners;

use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CartDoesNotIncludeRequiredProductsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CartIncludesDisallowedProductsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Exceptions\InvalidCouponTypeException;
use Bittacora\Bpanel4\Coupons\Services\CouponDiscountCalculator;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;

final class ApplyDiscountCouponsHook
{
    public static bool $running = false; // Para evitar recursividad, ya que este hook podría ser llamado otra vez
    // al calcular el total del carrito para calcular el descuento

    public function __construct(private readonly CouponDiscountCalculator $couponDiscountCalculator)
    {
    }

    /**
     * @throws InvalidPriceException
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CartDoesNotIncludeRequiredProductsException
     * @throws CartIncludesDisallowedProductsException
     * @throws CouponHasExpiredException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws InvalidCouponTypeException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     *
     * @param array{cartService: CartService, amount: Price} $array
     */
    public function handle(array &$array): void
    {
        if (self::$running) {
            return;
        }

        self::$running = true;
        $cart = $array['cartService']->getCart();
        foreach ($cart->getCoupons() as $coupon) {
            $discount = $this->couponDiscountCalculator->calculateDiscount($coupon, $cart);
            $array['amount'] = new Price($array['amount']->toFloat() - $discount->toFloat());
        }
        self::$running = false;
    }
}
