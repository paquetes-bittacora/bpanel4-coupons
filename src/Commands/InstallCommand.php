<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-coupons:install';

    /** @var string */
    protected $description = 'Instala el paquete de cupones descuento';

    private const PERMISSIONS = ['index', 'show', 'destroy', 'edit', 'update', 'create', 'store'];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->giveAdminPermissions();
        $this->createMenuEntries($adminMenu);
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        /** @var Role $adminRole */
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-coupons.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-coupons',
            'Cup. descuento',
            'far fa-badge-percent'
        );
        $adminMenu->createAction(
            'bpanel4-coupons',
            'Listado',
            'index',
            'fas fa-list'
        );
        $adminMenu->createAction(
            'bpanel4-coupons',
            'Nuevo',
            'create',
            'fas fa-plus'
        );
    }
}
