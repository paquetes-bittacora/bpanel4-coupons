<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CouponIsNotAvailableIfCartHasDiscountedProductsException extends NotApplicableCouponException
{
    /** @var string  */
    protected $message = 'El cupón no está disponible porque hay productos rebajados en el carrito';
}
