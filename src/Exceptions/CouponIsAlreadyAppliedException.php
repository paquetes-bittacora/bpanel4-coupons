<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CouponIsAlreadyAppliedException extends NotApplicableCouponException
{
    /** @var string $message */
    protected $message = 'El cupón ya se había aplicado al carrito';
}