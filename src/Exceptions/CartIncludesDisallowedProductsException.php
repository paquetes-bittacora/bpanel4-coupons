<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CartIncludesDisallowedProductsException extends NotApplicableCouponException
{
    /** @var string */
    protected $message = 'No se puede aplicar el cupón a alguno de los productos del carrito';
}
