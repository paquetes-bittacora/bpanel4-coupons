<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class ClientIsNotIncludedInCouponException extends NotApplicableCouponException
{
    /**
     * @var string $message
     */
    protected $message = 'El cupón no puede aplicarse porque no está permitido para el cliente actual';
}
