<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CartDoesNotIncludeRequiredProductsException extends NotApplicableCouponException
{
    /** @var string */
    public $message = 'No se puede aplicar el cupón porque el carrito no incluye los productos a los que es aplicable';
}
