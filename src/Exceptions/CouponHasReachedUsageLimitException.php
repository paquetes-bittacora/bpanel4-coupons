<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CouponHasReachedUsageLimitException extends NotApplicableCouponException
{
    /** @var string $message */
    protected $message = 'Se ha alcanzado el límite de veces que puede usarse el cupón';
}
