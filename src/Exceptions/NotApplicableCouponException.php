<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

use Exception;

abstract class NotApplicableCouponException extends Exception
{
}
