<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CartDoesNotIncludeRequiredCategoriesException extends NotApplicableCouponException
{
    /** @var string */
    protected $message = 'El producto no incluye productos de las categorías a las que se puede aplicar el cupón';
}
