<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CouponHasExpiredException extends NotApplicableCouponException
{
    /** @var string */
    protected $message = 'El cupón que intenta usar ha caducado';
}
