<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

use Exception;

final class InvalidCouponTypeException extends Exception
{
    /** @var string */
    protected $message = 'El tipo de cupón no es válido';
}
