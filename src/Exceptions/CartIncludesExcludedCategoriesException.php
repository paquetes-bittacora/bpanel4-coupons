<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CartIncludesExcludedCategoriesException extends NotApplicableCouponException
{
    /** @var string */
    protected $message = 'El cupón no se puede aplicar porque el carrito incluye categorías que están excluidas ' .
        'para este cupón';
}
