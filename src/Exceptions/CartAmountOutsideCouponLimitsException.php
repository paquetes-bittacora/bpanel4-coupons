<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Exceptions;

final class CartAmountOutsideCouponLimitsException extends NotApplicableCouponException
{
    /** @var string */
    protected $message = 'El total del carrito no se encuentra entre los límites establecidos para el cupón';
}
