<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed|string $discount
 * @property mixed|string $code
 * @property int|mixed $coupon_id
 * @property int|mixed $order_id
 */
final class OrderCoupon extends Model
{
    /**
     * @return mixed|string
     */
    public function getDiscount(): mixed
    {
        return $this->discount;
    }

    /**
     * @return mixed|string
     */
    public function getCode(): mixed
    {
        return $this->code;
    }
}
