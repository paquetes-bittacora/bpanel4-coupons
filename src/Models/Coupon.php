<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Models;

use Bittacora\Category\Models\CategoryModel;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Products\Models\Product;
use DateTime;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * \Bittacora\Bpanel4\Coupons\Models\Coupon
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $type
 * @property float $discount_amount
 * @property DateTime|null $expiration_date
 * @property float|null $min_cart_amount
 * @property float|null $max_cart_amount
 * @property bool|null $disallow_if_discounted_products
 * @property int|null $global_usages_limit
 * @property int|null $user_usages_limit
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Coupon newModelQuery()
 * @method static Builder|Coupon newQuery()
 * @method static Builder|Coupon query()
 * @method static Builder|Coupon whereCode($value)
 * @method static Builder|Coupon whereCreatedAt($value)
 * @method static Builder|Coupon whereDisallowIfDiscountedProducts($value)
 * @method static Builder|Coupon whereDiscountAmount($value)
 * @method static Builder|Coupon whereExpirationDate($value)
 * @method static Builder|Coupon whereGlobalUsagesLimit($value)
 * @method static Builder|Coupon whereId($value)
 * @method static Builder|Coupon whereMaxCartAmount($value)
 * @method static Builder|Coupon whereMinCartAmount($value)
 * @method static Builder|Coupon whereType($value)
 * @method static Builder|Coupon whereUpdatedAt($value)
 * @method static Builder|Coupon whereUserUsagesLimit($value)
 * @mixin Eloquent
 */
class Coupon extends Model
{
    public $guarded = ['id'];

    public $casts = [
        'expiration_date' => 'datetime:Y-m-d H:i'
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name ?? '';
    }

    public function getCode(): string
    {
        return $this->code ?? '';
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getType(): string
    {
        return $this->type ?? 'fixed';
    }

    public function getTypeName(): string {
        return match ($this->type) {
            'percentage' => 'Porcentaje',
            'fixed' => 'Cant. fija',
            'free-products' => 'Productos como regalo',
        };
    }

    public function getAvailableTypes(): array
    {
        return [
            'percentage' => 'Porcentaje del carrito',
            'fixed' => 'Cantidad fija',
            'free-products' => 'Productos como regalo',
        ];
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getDiscountAmount(): float
    {
        return $this->discount_amount ?? 0.0;
    }

    public function getDiscountAmountWithUnit(): string
    {
        $unit = match ($this->type) {
            'percentage' => '%',
            'fixed' => '€',
            'free-products' => '',
        };

        return $this->discount_amount . ' ' . $unit;
    }

    public function setDiscountAmount(float $discountAmount): void
    {
        $this->discount_amount = $discountAmount;
    }

    public function getExpirationDate(): ?DateTime
    {
        return $this->expiration_date;
    }

    public function setExpirationDate(?DateTime $expirationDate): void
    {
        $this->expiration_date = $expirationDate;
    }

    public function getMinCartAmount(): ?float
    {
        return $this->min_cart_amount;
    }

    public function setMinCartAmount(?float $minCartAmount): void
    {
        $this->min_cart_amount = $minCartAmount;
    }

    public function getMaxCartAmount(): ?float
    {
        return $this->max_cart_amount;
    }

    public function setMaxCartAmount(?float $maxCartAmount): void
    {
        $this->max_cart_amount = $maxCartAmount;
    }

    public function getDisallowIfDiscountedProducts(): bool
    {
        return (bool) $this->disallow_if_discounted_products;
    }

    public function setDisallowIfDiscountedProducts(?bool $disallowIfDiscountedProducts): void
    {
        $this->disallow_if_discounted_products = $disallowIfDiscountedProducts;
    }

    public function getGlobalUsagesLimit(): ?int
    {
        return $this->global_usages_limit;
    }

    public function setGlobalUsagesLimit(?int $globalUsagesLimit): void
    {
        $this->global_usages_limit = $globalUsagesLimit;
    }

    public function getUserUsagesLimit(): ?int
    {
        return $this->user_usages_limit;
    }

    public function setUserUsagesLimit(?int $userUsagesLimit): void
    {
        $this->user_usages_limit = $userUsagesLimit;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getExcludedProducts(): Collection
    {
        return $this->products()->wherePivot('action', 'exclude')->get();
    }

    /**
     * @return BelongsToMany<Product>
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'coupons_products');
    }

    /**
     * @return Collection<int, Product>
     */
    public function getRequiredProducts(): Collection
    {
        return $this->products()->withPivot(['quantity'])->wherePivot('action', 'include')->get();
    }

    /**
     * @return BelongsToMany<CategoryModel>
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(CategoryModel::class, 'coupons_categories');
    }

    public function addCategory(CategoryModel $category, string $action = 'include'): void
    {
        $this->categories()->save($category, ['action' => $action]);
    }

    /**
     * @return Collection<int, CategoryModel>
     */
    public function getRequiredCategories(): Collection
    {
        return $this->categories()->wherePivot('action', 'include')->get();
    }

    /**
     * @return Collection<int, CategoryModel>
     */
    public function getExcludedCategories(): Collection
    {
        return $this->categories()->wherePivot('action', 'exclude')->get();
    }

    /**
     * @return BelongsToMany<Client>
     */
    public function clients(): BelongsToMany
    {
        return $this->belongsToMany(Client::class, 'coupons_clients');
    }

    public function addClient(Client $couponClient): void
    {
        $this->clients()->save($couponClient);
    }

    /**
     * @return Collection<int, Client>
     */
    public function getAllowedClients(): Collection
    {
        return $this->clients()->get();
    }

    /**
     * @return BelongsToMany<Client>
     */
    public function usages(): BelongsToMany
    {
        return $this->belongsToMany(Client::class, 'coupons_usages');
    }

    public function registerUsage(Client $client): void
    {
        $this->usages()->save($client);
    }

    public function setUserUsageLimit(?int $limit): void
    {
        $this->user_usages_limit = $limit;
    }

    public function getUserUsageLimit(): ?int
    {
        return $this->user_usages_limit;
    }


    public function setGlobalUsageLimit(?int $limit): void
    {
        $this->global_usages_limit = $limit;
    }

    public function getGlobalUsageLimit(): ?int
    {
        return $this->global_usages_limit;
    }
}
