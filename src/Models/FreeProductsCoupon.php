<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Models;

use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

final class FreeProductsCoupon extends Coupon
{
    public $table = 'coupons';

    /**
     * @return BelongsToMany<Product>
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'coupons_products', 'coupon_id', 'product_id');
    }

    public function giftedProducts(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'coupons_gifted_products', 'coupon_id', 'product_id');
    }

    public function getRequiredProducts(): Collection
    {
        return $this->products()->withPivot('quantity')->get();
    }

    public function getGiftedProducts(): Collection
    {
        return $this->giftedProducts()->withPivot('quantity')->get();
    }

    /**
     * @return BelongsToMany<Client>
     */
    public function usages(): BelongsToMany
    {
        return $this->belongsToMany(Client::class, 'coupons_usages', 'coupon_id');
    }
}