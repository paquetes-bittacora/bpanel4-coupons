<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Coupons\Http\Controllers\Bpanel4CouponsAdminController;
use Bittacora\Bpanel4\Coupons\Http\Controllers\Bpanel4CouponsPublicController;
use Illuminate\Support\Facades\Route;
use Bittacora\Bpanel4\Coupons\Http\Controllers\Bpanel4CouponsFreeProductAdminController;

Route::prefix('bpanel/cupones-descuento')->name('bpanel4-coupons.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/listar', [Bpanel4CouponsAdminController::class, 'index'])->name('index');
        Route::get('/nuevo', [Bpanel4CouponsAdminController::class, 'create'])->name('create');
        Route::post('/nuevo', [Bpanel4CouponsAdminController::class, 'store'])->name('store');
        Route::get('/{coupon}/edit', [Bpanel4CouponsAdminController::class, 'edit'])->name('edit');
        Route::put('/{coupon}/edit', [Bpanel4CouponsAdminController::class, 'update'])->name('update');
        Route::delete('/{coupon}/destroy', [Bpanel4CouponsAdminController::class, 'destroy'])->name('destroy');
    });

Route::prefix('bpanel/cupones-descuento/producto-gratis')->name('bpanel4-coupons.free-products.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/listar', [Bpanel4CouponsAdminController::class, 'index'])->name('index');
        Route::get('/nuevo', [Bpanel4CouponsFreeProductAdminController::class, 'create'])->name('create');
        Route::post('/nuevo', [Bpanel4CouponsFreeProductAdminController::class, 'store'])->name('store');
        Route::get('/{coupon}/editar', [Bpanel4CouponsFreeProductAdminController::class, 'edit'])->name('edit');
        Route::put('/{coupon}/editar', [Bpanel4CouponsFreeProductAdminController::class, 'update'])->name('update');
        Route::delete('/{coupon}/destroy', [Bpanel4CouponsFreeProductAdminController::class, 'destroy'])->name('destroy');
    });


Route::prefix('/cupones-descuento')->name('bpanel4-coupons.public.')->middleware(['web'])->group(function () {
    Route::post('/aplicar', [Bpanel4CouponsPublicController::class, 'apply'])->name('apply');
    Route::get('/{coupon}/quitar', [Bpanel4CouponsPublicController::class, 'removeCouponFromCart'])->name('remove-coupon-from-cart');
});
