<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'coupons_carts';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('coupon_id');
            $table->unsignedBigInteger('cart_id');
            $table->timestamps();

            $table->foreign('coupon_id')->references('id')->on('coupons');
            $table->foreign('cart_id')->references('id')->on('carts');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
