<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'coupons';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->string('code');
            $table->string('type'); // percentage o fixed (otros paquetes pueden usar lo que quieran)
            $table->float('discount_amount');
            $table->timestamp('expiration_date')->nullable();
            $table->float('min_cart_amount')->nullable();
            $table->float('max_cart_amount')->nullable();
            $table->boolean('disallow_if_discounted_products')->nullable();
            $table->unsignedInteger('global_usages_limit')->nullable();
            $table->unsignedInteger('user_usages_limit')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
