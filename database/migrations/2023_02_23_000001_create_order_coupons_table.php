<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'order_coupons';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('coupon_id');
            $table->string('code');
            $table->string('discount');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('coupon_id')->references('id')->on('coupons');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
