<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Database\Factories;

use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Bittacora\Bpanel4\Products\Models\Product;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method FreeProductsCoupon createOne($attributes = [])
 *
 * @extends Factory<Coupon>
 */
final class FreeProductsCouponFactory extends Factory
{
    /** @var string */
    protected $model = FreeProductsCoupon::class;

    /**
     * @return array<string, int>|array<string, string>|array<string, array>
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'code' => bin2hex(random_bytes(4)),
            'type' => 'free-products',
            'discount_amount' => 0,
        ];
    }

    public function withExpirationDate(DateTime $dateTime): FreeProductsCouponFactory
    {
        return $this->state(fn(): array => [
            'expiration_date' => $dateTime,
        ]);
    }

    /**
     * @param array<array{product: Product, quantity: int}> $giftedProducts
     */
    public function withGiftedProducts(array $giftedProducts): FreeProductsCouponFactory
    {
        $factory = $this;
        foreach ($giftedProducts as $product) {
            $factory = $factory->hasAttached(
                $product['product'],
                [
                    'quantity' => $product['quantity'],
                ],
                'giftedProducts'
            );
        }

        return $factory;
    }

    /**
     * @param array<array{product: Product, quantity: int}> $requiredProducts
     */
    public function withRequiredProducts(array $requiredProducts): FreeProductsCouponFactory
    {
        $factory = $this;
        foreach ($requiredProducts as $product) {
            $factory = $factory->hasAttached($product['product'], [
                'quantity' => $product['quantity'],
                'action' => 'include',
            ], 'products');
        }

        return $factory;
    }

    public function withUserUsageLimit(int $usageLimit): FreeProductsCouponFactory
    {
        return $this->state(fn(): array => [
            'user_usages_limit' => $usageLimit,
        ]);
    }

    public function withGlobalUsageLimit(int $usageLimit): FreeProductsCouponFactory
    {
        return $this->state(fn(): array => [
            'global_usages_limit' => $usageLimit,
        ]);
    }
}
