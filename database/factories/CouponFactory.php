<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Database\Factories;

use Bittacora\Bpanel4\Coupons\Models\Coupon;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method Coupon createOne($attributes = [])
 *
 * @extends Factory<Coupon>
 */
final class CouponFactory extends Factory
{
    /** @var string  */
    protected $model = Coupon::class;

    /**
     * @return array<string, int>|array<string, string>|array<string, array>
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'code' => bin2hex(random_bytes(4)),
            'type' => 'percentage',
            'discount_amount' => random_int(5, 30),
        ];
    }

    public function withPercentageDiscount(float $percentage): CouponFactory
    {
        return $this->state(fn (): array => [
            'discount_amount' => $percentage,
            'type' => 'percentage',
        ]);
    }

    public function withFixedDiscount(float $amount): CouponFactory
    {
        return $this->state(fn (): array => [
            'discount_amount' => $amount,
            'type' => 'fixed',
        ]);
    }

    public function withExpirationDate(DateTime $dateTime): CouponFactory
    {
        return $this->state(fn (): array => [
            'expiration_date' => $dateTime,
        ]);
    }

    public function withMinimumAmount(int $minimum): CouponFactory
    {
        return $this->state(fn (): array => [
            'min_cart_amount' => $minimum,
        ]);
    }

    public function withMaximumAmount(int $maximum): CouponFactory
    {
        return $this->state(fn (): array => [
            'max_cart_amount' => $maximum,
        ]);
    }

    public function doesNotAllowDiscountedProducts(): CouponFactory
    {
        return $this->state(fn (): array => [
            'disallow_if_discounted_products' => true,
        ]);
    }

    public function withUserUsageLimit(int $usageLimit): CouponFactory
    {
        return $this->state(fn (): array => [
            'user_usages_limit' => $usageLimit,
        ]);
    }

    public function withGlobalUsageLimit(int $usageLimit): CouponFactory
    {
        return $this->state(fn (): array => [
            'global_usages_limit' => $usageLimit,
        ]);
    }
}
