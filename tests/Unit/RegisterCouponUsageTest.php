<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit;

use Bittacora\Bpanel4\Coupons\Actions\RegisterCouponUsage;
use Bittacora\Bpanel4\Coupons\Database\Factories\CouponFactory;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class RegisterCouponUsageTest extends TestCase
{
    use RefreshDatabase;

    private RegisterCouponUsage $action;

    protected function setUp(): void
    {
        parent::setUp();

        $this->action = $this->app->make(RegisterCouponUsage::class);
    }

    public function testSeRegistraQueElCuponHaSidoUsado(): void
    {
        $coupon = (new CouponFactory())->createOne();
        $client = (new ClientFactory())->createOne();
        $this->action->execute($client, $coupon);

        $this->assertDatabaseHas('coupons_usages', [
            'client_id' => $client->getClientId(),
            'coupon_id' => $coupon->getId(),
        ]);
    }
}
