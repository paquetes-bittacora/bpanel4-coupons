<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit\Http\Livewire;

use Bittacora\Bpanel4\Coupons\Database\Factories\CouponFactory;
use Bittacora\Bpanel4\Coupons\Http\Livewire\CouponsDatatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

/**
 * Planteo este TestCase de forma un poco genérica con la idea de poder testear los datatable fácilmente en el futuro,
 * simplemente configurando un poco para cada caso concreto.
 */
final class CouponsDatatableTest extends TestCase
{
    use RefreshDatabase;

    protected string $datatableClass;
    /** @var array<string>  */
    protected array $columns;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setDatatableClass();
        $this->setColumns();
    }

    public function testSeMuestranLasColumnas(): void
    {
        // Arrange

        // Act
        $component = Livewire::test($this->datatableClass);

        // Assert
        foreach ($this->columns as $column) {
           $component->assertSee($column);
        }
    }

    public function testSeMuestraElBotonParaBorrarEnMasa(): void
    {
        // Arrange

        // Act
        $component = Livewire::test($this->datatableClass);

        // Assert
        $component->assertSeeHtml('wire:click="bulkDelete"');
    }

    protected function setDatatableClass(): void
    {
        $this->datatableClass = CouponsDatatable::class;
    }

    protected function setColumns(): void
    {
        $this->columns = [
            'Código',
            'Tipo',
            'Cantidad',
            'Caducidad',
            'Lím. uso/usuario',
            'Lím. uso/global',
            'Fecha de creación',
            'Acciones'
        ];
    }

    /**
     * Devolver una colección de elementos que saldrán en el datatable
     * @return Collection
     */
    protected function createRows(): Collection
    {
        return (new CouponFactory())->count(3)->create();
    }
}