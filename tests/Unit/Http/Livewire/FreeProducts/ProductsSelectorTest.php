<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit\Http\Livewire\FreeProducts;

use Bittacora\Bpanel4\Coupons\Http\Livewire\FreeProducts\ProductsSelector;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use PHPUnit\Framework\Attributes\CoversClass;
use Tests\TestCase;

#[CoversClass(ProductsSelector::class)]
final class ProductsSelectorTest extends TestCase
{
    use RefreshDatabase;

    public function testSoloSeMuestranLosProductosActivos(): void
    {
        // Arrange
        $activeProduct = (new ProductFactory())->createOne();
        $inactiveProduct = (new ProductFactory())->createOne();
        $inactiveProduct->setActive(false);
        $inactiveProduct->save();

        // Act
        $component = Livewire::test(ProductsSelector::class);
        $component->call('addProduct');

        // Assert
        $component->assertSeeHtml(htmlentities($activeProduct->name));
        $component->assertDontSeeHtml(htmlentities($inactiveProduct->name));
    }

    public function testAlAnadirUnNuevoProductoSeInicializaConLosValoresCorrectos(): void
    {
        // Arrange
        (new ProductFactory())->createOne();

        // Act
        $component = Livewire::test(ProductsSelector::class);
        $component->call('addProduct');

        // Assert
        $componentProducts = $component->get('products');
        $this->assertEquals(0, $componentProducts[0]['quantity']);
        $this->assertNull($componentProducts[0]['productId']);
    }

    public function testSePuedeEliminarUnProducto(): void
    {
        // Arrange

        // Act
        $component = Livewire::test(ProductsSelector::class);
        $component->call('addProduct');
        $component->call('removeProduct', 0);

        // Assert
        $componentProducts = $component->get('products');
        $this->assertEmpty($componentProducts);
    }
}