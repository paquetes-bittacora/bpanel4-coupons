<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit\Http\Livewire;

use Bittacora\Bpanel4\Coupons\Http\Livewire\FreeProducts\ProductsSelector;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Livewire;

final class CouponProductsSelectorTest extends TestCase
{
    use RefreshDatabase;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    public function testElComponenteSeRenderiza(): void
    {
        // Arrange

        // Act
        $component = Livewire::test(ProductsSelector::class);

        // Assert
        $component->assertSee('Añadir producto');
    }

    public function testAlPulsarElBotonParaAnadirUnProductoSeCreaUnaNuevaFilaEnElArray(): void
    {
        // Arrange

        // Act
        $component = Livewire::test(ProductsSelector::class);
        $component->call('addProduct');

        // Assert
        $component->assertSee('Producto');
        $component->assertSee('Cantidad');
    }

    public function testSeSeleccionanLosProductosQueSeIndicanComoSeleccionados(): void
    {
        // Arrange
        $products = (new ProductFactory())->count(5)->create()->pluck('name', 'id')->toArray();

        // Act
        $component = Livewire::test(ProductsSelector::class, [
            'availableProducts' => $products,
            'products' => [
                ['productId' => 2, 'quantity' => 1],
            ]
        ]);
        // Assert
        // Hay que respetar los espacios porque si no fallará el assert
        $component->assertSeeHtml('value="2"  selected');
    }
}