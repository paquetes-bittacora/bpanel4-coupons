<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit\Http\Controllers;

use Bittacora\Bpanel4\Coupons\Database\Factories\FreeProductsCouponFactory;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use DateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Random\RandomException;
use Tests\TestCase;

final class Bpanel4CouponsFreeProductAdminControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    public function testMuestraLaVistaDeCreacionDelCupon(): void
    {
        // Arrange
        (new AdminHelper())->actingAsAdminWithPermissions(['bpanel4-coupons.create']);

        // Act
        $result = $this->get(route('bpanel4-coupons.free-products.create'));

        // Assert
        $result->assertOk();
        $result->assertSee('Productos que deben estar en el carrito');
    }

    public function testSePuedeCrearUnCuponConProductosRegalo(): void
    {
        // Arrange
        (new AdminHelper())->actingAsAdminWithPermissions(['bpanel4-coupons.store', 'bpanel4-coupons.index']);
        (new ProductFactory())->count(6)->create();

        // Act
        $code = $this->faker->lexify('??????');
        $result = $this->followingRedirects()->post(route('bpanel4-coupons.free-products.store'), [
            'name' => $this->faker->name,
            'code' => $code,
            'expiration_date' => (new DateTime('+1 month'))->format('d/m/Y H:i'),
            'user_usages_limit' => 1,
            'global_usages_limit' => 100,
            'required_products' => [],
            'gifted_products' => [],
            'save' => true,
        ]);

        // Assert
        $result->assertOk();
        $this->assertDatabaseHas(FreeProductsCoupon::class, ['code' => $code]);
    }

    public function testMuestraLaVistaDeEdicionDelCupon(): void
    {
        // Arrange
        $coupon = (new FreeProductsCouponFactory())->createOne();
        (new AdminHelper())->actingAsAdminWithPermissions(['bpanel4-coupons.edit']);

        // Act
        $result = $this->get(route('bpanel4-coupons.free-products.edit', ['coupon' => $coupon]));

        // Assert
        $result->assertOk();
        $result->assertSee($coupon->name);
    }

    /**
     * @throws RandomException
     */
    public function testActualizaUnCupon(): void
    {
        // Arrange
        $coupon = (new FreeProductsCouponFactory())->createOne();
        (new AdminHelper())->actingAsAdminWithPermissions(['bpanel4-coupons.update']);
        $code = bin2hex(random_bytes(4));

        // Act
        $this->put(route('bpanel4-coupons.free-products.update', ['coupon' => $coupon]), [
            'name' => $this->faker->name,
            'code' => $code,
            'expiration_date' => (new DateTime('+2 months'))->format('d/m/Y H:i'),
            'user_usages_limit' => 9,
            'global_usages_limit' => 99,
            'required_products' => [],
            'gifted_products' => [],
            'save' => true,
        ]);

        // Assert
        $this->assertDatabaseHas(FreeProductsCoupon::class, [
            'id' => $coupon->id,
            'code' => $code,
            'user_usages_limit' => 9,
            'global_usages_limit' => 99,
        ]);
    }
}