<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit\Actions\FreeProducts;

use Bittacora\Bpanel4\Coupons\Actions\FreeProducts\CreateFreeProductsCoupon;
use Bittacora\Bpanel4\Coupons\Dtos\FreeProductsCouponDto;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use DateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Random\RandomException;
use Tests\TestCase;

final class CreateFreeProductsCouponTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    private CreateFreeProductsCoupon $createFreeProductsCoupon;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createFreeProductsCoupon = $this->app->make(CreateFreeProductsCoupon::class);
    }

    /**
     * @throws RandomException
     */
    public function testCreaUnCuponConProductosGratis(): void
    {
        // Arrange

        // Act
        $testData = $this->getTestData();
        $code = $testData['code'];
        $this->createFreeProductsCoupon->execute(new FreeProductsCouponDto(...$testData));

        // Assert
        $this->assertDatabaseHas(FreeProductsCoupon::class, ['code' => $code]);
    }

    /**
     * @throws RandomException
     */
    public function testSeGuardanLosProductosRequeridosParaAplicarElCupon(): void
    {
        // Arrange
        $products = (new ProductFactory())->count(3)->create();

        // Act
        $testData = $this->getTestData();
        $testData['requiredProducts'] = [
            ['id' => $products[0]->id, 'quantity' => 1],
            ['id' => $products[2]->id, 'quantity' => 1],
        ];
        $coupon = $this->createFreeProductsCoupon->execute(new FreeProductsCouponDto(...$testData));

        // Assert
        $this->assertCount(2, $coupon->getRequiredProducts());
    }

    /**
     * @throws RandomException
     */
    public function testSeGuardanLosProductosRegaladosAlAplicarElCupon(): void
    {
        // Arrange
        $products = (new ProductFactory())->count(3)->create();

        // Act
        $testData = $this->getTestData();
        $testData['giftedProducts'] = [
            ['id' => $products[0]->id, 'quantity' => 1],
            ['id' => $products[2]->id, 'quantity' => 1],
        ];
        $coupon = $this->createFreeProductsCoupon->execute(new FreeProductsCouponDto(...$testData));

        // Assert
        $this->assertCount(2, $coupon->getGiftedProducts());
    }

    /**
     * @throws RandomException
     */
    private function getTestData(): array
    {
        return [
            'name' => $this->faker->name(),
            'code' => $this->faker->lexify('??????'),
            'expirationDate' => new DateTime('+2 months'),
            'requiredProducts' => [],
            'giftedProducts' => [],
            'userUsagesLimit' => random_int(0, 1000),
            'globalUsagesLimit' => random_int(0, 1000),
        ];
    }
}