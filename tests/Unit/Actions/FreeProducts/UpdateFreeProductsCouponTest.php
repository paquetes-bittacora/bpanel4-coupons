<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Unit\Actions\FreeProducts;

use Bittacora\Bpanel4\Coupons\Actions\FreeProducts\UpdateFreeProductsCoupon;
use Bittacora\Bpanel4\Coupons\Database\Factories\FreeProductsCouponFactory;
use Bittacora\Bpanel4\Coupons\Dtos\FreeProductsCouponDto;
use Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Random\RandomException;
use Tests\TestCase;

final class UpdateFreeProductsCouponTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    private UpdateFreeProductsCoupon $updateFreeProductsCoupon;

    protected function setUp(): void
    {
        parent::setUp();
        $this->updateFreeProductsCoupon = resolve(UpdateFreeProductsCoupon::class);
    }

    /**
     * @throws RandomException
     */
    public function testActualizaUnCupon(): void
    {
        // Arrange
        $coupon = (new FreeProductsCouponFactory())->createOne();
        $testData = $this->getTestData();

        // Act
        $this->updateFreeProductsCoupon->execute($coupon, new FreeProductsCouponDto(...$testData));

        // Assert
        $this->assertDatabaseHas(FreeProductsCoupon::class, [
            'id' => $coupon->id,
            'code' => $testData['code'],
            'expiration_date' => $testData['expirationDate'],
        ]);
    }

    /**
     * @throws RandomException
     */
    private function getTestData(): array
    {
        return [
            'name' => $this->faker->name(),
            'code' => $this->faker->lexify('??????'),
            'expirationDate' => new \DateTime('+2 months'),
            'requiredProducts' => [],
            'giftedProducts' => [],
            'userUsagesLimit' => random_int(0, 1000),
            'globalUsagesLimit' => random_int(0, 1000),
        ];
    }
}