<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Integration;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Contracts\CouponUsageRepository;
use Bittacora\Bpanel4\Coupons\Database\Factories\CouponFactory;
use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\InvalidCouponTypeException;
use Bittacora\Bpanel4\Coupons\Services\CouponDiscountCalculator;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use Bittacora\Bpanel4\Orders\Factories\CartServiceFactory;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\MonetaryAmount;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

final class CouponsDiscountCalculatorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws InvalidPriceException
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     * @throws InvalidCouponTypeException
     */
    public function testCalculaDescuentoPorPorcentaje(): void
    {
        $cartServiceFactoryMock = $this->givenWeHaveACartWithATotalOf(100);
        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, new CouponValidator(
            $this->app->make(CouponUsageRepository::class),
            $this->app->make(ClientService::class),
        ));
        $coupon = (new CouponFactory())->withPercentageDiscount(10.0)->createOne();

        $discount = $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));

        self::assertEquals((new MonetaryAmount(10))->toFloat(), $discount->toFloat());
    }

    /**
     * @throws Exception
     */
    public function testSePuedeAplicarUnDescuentoPorUnaCantidadFija(): void
    {
        $cartServiceFactoryMock = $this->givenWeHaveACartWithATotalOf(500);
        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, new CouponValidator(
            $this->app->make(CouponUsageRepository::class),
            $this->app->make(ClientService::class),
        ));
        $couponDiscount = random_int(10, 20);
        $coupon = (new CouponFactory())->withFixedDiscount($couponDiscount)->createOne();

        $discount = $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));

        self::assertEquals((new MonetaryAmount($couponDiscount))->toInt(), $discount->toInt());
    }

    private function givenWeHaveACartWithATotalOf(int $cartAmount): \Mockery\LegacyMockInterface
    {
        $cartServiceFactoryMock = Mockery::mock(CartServiceFactory::class);
        $cartServiceMock = Mockery::mock(CartService::class);
        $cartServiceMock->shouldReceive('getCartTotal')->andReturn(new Price($cartAmount));
        $cartServiceMock->shouldReceive('hasDiscountedProducts')->andReturnFalse();
        $cartServiceFactoryMock->shouldReceive('getCartService')->andReturn($cartServiceMock);
        $cartServiceMock->shouldReceive('getCartTotalWithoutTaxesBeforeDiscounts')->andReturn(new Price($cartAmount));
        return $cartServiceFactoryMock;
    }
}
