<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Integration;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Coupons\Database\Factories\FreeProductsCouponFactory;
use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasReachedUsageLimitException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsAlreadyAppliedException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Services\CouponApplier;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use Bittacora\Bpanel4\Orders\Contracts\CartableProduct;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Orders\Tests\Integration\Helpers\InitializeBasicCartForClient;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Models\CartProductFactory;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\Products\Presenters\ProductPresenterFactory;
use Bittacora\LivewireCountryStateSelector\Database\Factories\CountryFactory;
use Bittacora\LivewireCountryStateSelector\Database\Factories\StateFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Random\RandomException;
use Tests\TestCase;
use function random_int;

/**
 * TestCase para comprobar el funcionamiento de los cupones con productos gratis en el carrito
 */
final class FreeProductsCouponsCartTest extends TestCase
{
    use RefreshDatabase;
    private ClientService $clientService;
    private CouponApplier $couponApplier;
    private CartService $cartService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->clientService = $this->app->make(ClientService::class);
        ['cartService' => $this->cartService] = (new InitializeBasicCartForClient())->execute($this->app);
        $this->couponApplier = new CouponApplier(
            $this->clientService,
            $this->app->make(CouponValidator::class),
            $this->cartService,
        );
        (new CountryFactory())->withName('España')->createOne();
        (new StateFactory())->withName('Badajoz')->createOne();
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws RandomException
     * @throws UserNotLoggedInException
     */
    public function testAlAnadirUnCuponAlCarritoSinLosProductosSeleccionadosNoSeAnadenLosProductosRegalo(): void
    {
        // Arrange
        $this->expectExceptionMessage('El carrito no incluye los productos obligatorios para aplicar el cupón');
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($this->getRequiredProducts())
            ->withGiftedProducts($this->getGiftedProducts())
            ->createOne();

        // Act
        $this->couponApplier->apply($coupon->code);
        $cart = $this->clientService->getClientCart();

        // Assert
    }

    /**
     * @throws RandomException
     * @throws UserNotLoggedInException
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     */
    public function testAlAnadirUnCuponAlCarritoSinLosProductosSeleccionadosNoSeAnadeElCupon(): void
    {
        // Arrange
        $this->expectExceptionMessage('El carrito no incluye los productos obligatorios para aplicar el cupón');
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($this->getRequiredProducts())
            ->withGiftedProducts($this->getGiftedProducts())
            ->createOne();

        // Act
        $this->couponApplier->apply($coupon->code);
        $this->clientService->getClientCart();

        // Assert
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws RandomException
     * @throws UserNotLoggedInException
     */
    public function testAlAnadirUnCuponAlCarritoConLosProductosSeleccionadosSeAnadenLosProductosRegalo(): void
    {
        // Arrange
        $giftedProducts = $this->getGiftedProducts();
        $requiredProducts = $this->getRequiredProducts();
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($requiredProducts)
            ->withGiftedProducts($giftedProducts)
            ->createOne();

        // Act
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);
        $this->addRequiredProductsToCart($cart, $requiredProducts);
        $this->couponApplier->apply($coupon->code);

        // Assert
        $this->assertEquals($giftedProducts[0]['product']->name, $cart->getGiftedProducts()[0]->getName());
        $this->assertEquals($giftedProducts[0]['quantity'], $cart->getGiftedProducts()[0]->pivot->quantity);
        $this->assertEquals($giftedProducts[1]['product']->name, $cart->getGiftedProducts()[1]->getName());
        $this->assertEquals($giftedProducts[1]['quantity'], $cart->getGiftedProducts()[1]->pivot->quantity);
    }

    /**
     * @throws CouponHasReachedUsageLimitException
     * @throws RandomException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws CouponHasExpiredException
     * @throws UserNotLoggedInException
     * @throws CartAmountOutsideCouponLimitsException
     */
    public function testNoSePuedeAnadirUnCuponCaducadoAlCarrito(): void
    {
        $this->expectException(CouponHasExpiredException::class);

        // Arrange
        $requiredProducts = $this->getRequiredProducts();
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($requiredProducts)
            ->withGiftedProducts($this->getGiftedProducts())
            ->createOne();
        $this->expireCoupon($coupon);
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);
        $this->addRequiredProductsToCart($cart, $requiredProducts);

        // Act
        $this->couponApplier->apply($coupon->code);
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws RandomException
     * @throws UserNotLoggedInException
     */
    public function testUnUsuarioNoPuedeUsarElCuponMasVecesDeLasConfiguradas(): void
    {
        $this->expectException(CouponHasReachedUsageLimitException::class);
        // Arrange
        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());
        $limit = random_int(1, 20);
        // Creo un cupón y un carrito para probar
        $requiredProducts = $this->getRequiredProducts();
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($requiredProducts)
            ->withGiftedProducts($this->getGiftedProducts())
            ->withUserUsageLimit($limit)
            ->createOne();
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);
        $this->addRequiredProductsToCart($cart, $requiredProducts);
        // Gasto todos los usos permitidos del cupón
        for ($i=1; $i<=$limit; ++$i) {
            $coupon->registerUsage($client);
        }

        // Act
        $this->couponApplier->apply($coupon->code);

        // Assert
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws RandomException
     * @throws UserNotLoggedInException
     */
    public function testUnUsuarioNoPuedeUsarElCuponMasVecesDeLasConfiguradasGlobalmente(): void
    {
        $this->expectException(CouponHasReachedUsageLimitException::class);
        // Arrange
        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());
        $limit = random_int(1, 20);
        // Creo un cupón y un carrito para probar
        $requiredProducts = $this->getRequiredProducts();
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($requiredProducts)
            ->withGiftedProducts($this->getGiftedProducts())
            ->withGlobalUsageLimit($limit)
            ->createOne();
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);
        $this->addRequiredProductsToCart($cart, $requiredProducts);
        // Gasto todos los usos permitidos del cupón
        for ($i=1; $i<=$limit; ++$i) {
            $coupon->registerUsage((new ClientFactory())->createOne());
        }

        // Act
        $this->couponApplier->apply($coupon->code);

        // Assert
    }

    /**
     * @throws CartAmountOutsideCouponLimitsException
     * @throws CouponHasExpiredException
     * @throws CouponHasReachedUsageLimitException
     * @throws CouponIsAlreadyAppliedException
     * @throws CouponIsNotAvailableIfCartHasDiscountedProductsException
     * @throws RandomException
     * @throws UserNotLoggedInException
     */
    public function testsNoSePuedeAplicar2VecesElMismoCuponAlCarrito(): void
    {
        $this->expectException(CouponIsAlreadyAppliedException::class);

        // Arrange
        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());
        // Creo un cupón y un carrito para probar
        $requiredProducts = $this->getRequiredProducts();
        $coupon = (new FreeProductsCouponFactory())
            ->withRequiredProducts($requiredProducts)
            ->withGiftedProducts($this->getGiftedProducts())
            ->createOne();
        $cart = $this->clientService->getClientCart();
        $this->cartService->setCart($cart);
        $this->addRequiredProductsToCart($cart, $requiredProducts);

        // Act
        $this->couponApplier->apply($coupon->code);
        $this->couponApplier->apply($coupon->code);

        // Assert
    }

    /**
     * @return array<array{product: Product, quantity: int}>
     * @throws RandomException
     */
    private function getRequiredProducts(): array
    {
        return [
            ['product' => (new ProductFactory())->createOne(), 'quantity' => random_int(2, 10)],
            ['product' => (new ProductFactory())->createOne(), 'quantity' => random_int(1, 10)],
        ];
    }

    /**
     * @return array<array{product: Product, quantity: int}>
     * @throws RandomException
     */
    private function getGiftedProducts(): array
    {
        return [
            ['product' => (new ProductFactory())->createOne(), 'quantity' => random_int(1, 10)],
            ['product' => (new ProductFactory())->createOne(), 'quantity' => random_int(1, 10)],
        ];
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function addRequiredProductsToCart(Cart $cart, array $requiredProducts): void
    {
        foreach ($requiredProducts as $requiredProduct) {
            $cart->addProduct($this->getCartableProduct($requiredProduct['product']), $requiredProduct['quantity']);
        }
    }

    /**
     * @throws UserNotLoggedInException
     */
    private function getCartableProduct($giftedProduct): CartableProduct
    {
        $cartProductFactory = new CartProductFactory($this->clientService, $this->app->make(ProductPresenterFactory::class));
        $cartProductFactory->setProduct($giftedProduct);
        return $cartProductFactory->make();
    }

    private function expireCoupon(\Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon $coupon): void
    {
        $coupon->expiration_date = new \DateTime('1 second ago');
        $coupon->save();
    }

    private function getInactiveCoupon(): \Bittacora\Bpanel4\Coupons\Models\FreeProductsCoupon
    {
        $coupon = (new FreeProductsCouponFactory())->createOne();
        $coupon->active = false;
        $coupon->save();
        return $coupon;
    }
}