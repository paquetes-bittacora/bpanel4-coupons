<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Integration;

use Bittacora\Bpanel4\Coupons\Database\Factories\CouponFactory;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Routing\UrlGenerator;
use Tests\TestCase;

final class Bpanel4CouponsAdminControllerTest extends TestCase
{
    use RefreshDatabase;

    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        $this->withoutExceptionHandling();
        $this->actingAs((new UserFactory())->withPermissions('bpanel4-coupons.edit')->createOne());
    }

    public function testSePuedeVerLaFichaDeEdicionDeUnCupon(): void
    {
        $coupon = (new CouponFactory())->createOne();
        $response = $this->get($this->urlGenerator->route('bpanel4-coupons.edit', ['coupon' => $coupon]));
        $response->assertOk();
    }
}
