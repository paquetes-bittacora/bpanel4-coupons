<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Integration;

use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Coupons\Contracts\CouponUsageRepository;
use Bittacora\Bpanel4\Coupons\Database\Factories\CouponFactory;
use Bittacora\Bpanel4\Coupons\Exceptions\CartAmountOutsideCouponLimitsException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasExpiredException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponHasReachedUsageLimitException;
use Bittacora\Bpanel4\Coupons\Exceptions\CouponIsNotAvailableIfCartHasDiscountedProductsException;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4\Coupons\Services\CouponDiscountCalculator;
use Bittacora\Bpanel4\Coupons\Services\CouponValidator;
use Bittacora\Bpanel4\Orders\Factories\CartServiceFactory;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Services\CartService;
use Bittacora\Bpanel4\Prices\Types\Price;
use DateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;

final class CouponValidatorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testNoSePuedeUsarUnCuponDespuesDeSuFechaDeCaducidad(): void
    {
        self::expectException(CouponHasExpiredException::class);

        $coupon = (new CouponFactory())->withExpirationDate(new DateTime('10 days ago'))->createOne();

        $cartServiceFactoryMock = $this->givenWeHaveACartWithATotalOf(500);
        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, $this->getCouponValidator());
        $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));
    }

    public function testNoSePuedeUsarUnCuponSiElImporteDelCarritoNoAlcanzaUnMinimo(): void
    {
        self::expectException(CartAmountOutsideCouponLimitsException::class);

        $coupon = (new CouponFactory())->withMinimumAmount(999)->createOne();

        $cartServiceFactoryMock = $this->givenWeHaveACartWithATotalOf(500);
        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, $this->getCouponValidator());
        $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));
    }

    public function testNoSePuedeUsarUnCuponSiElImporteDelCarritoSuperaUnMaximo(): void
    {
        self::expectException(CartAmountOutsideCouponLimitsException::class);

        $coupon = (new CouponFactory())->withMaximumAmount(99)->createOne();

        $cartServiceFactoryMock = $this->givenWeHaveACartWithATotalOf(100);
        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, $this->getCouponValidator());
        $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));
    }

    public function testNoSePermiteAplicarSiHayProductosRebajadosYNoEstanPermitidos(): void
    {
        self::expectException(CouponIsNotAvailableIfCartHasDiscountedProductsException::class);

        $coupon = (new CouponFactory())->doesNotAllowDiscountedProducts()->createOne();

        $cartServiceFactoryMock = $this->givenWeHaveACartWithDiscountedProducts();
        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, $this->getCouponValidator());
        $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));
    }

    public function testUnClienteNoPuedeUsarUnCuponMasVecesDeLasIndicadasEnElPanel(): void
    {
        self::expectException(CouponHasReachedUsageLimitException::class);

        $coupon = (new CouponFactory())->withUserUsageLimit(1)->createOne();

        $cartServiceFactoryMock = $this->givenWeHaveACartWithACouponApplied($coupon);

        $couponUsageRepository = Mockery::mock(CouponUsageRepository::class);
        $couponUsageRepository->shouldIgnoreMissing();
        $couponUsageRepository->shouldReceive('getCouponUsagesForClient')->andReturn([[], [], []]);

        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, new CouponValidator(
            $couponUsageRepository,
            Mockery::mock(ClientService::class)->shouldIgnoreMissing(),
        ));

        $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));
    }

    public function testNoSePuedeUsarUnCuponMasVecesDeLasIndicadasEnElPanelGlobalmente(): void
    {
        self::expectException(CouponHasReachedUsageLimitException::class);

        $coupon = (new CouponFactory())->withGlobalUsageLimit(2)->createOne();

        $cartServiceFactoryMock = $this->givenWeHaveACartWithACouponApplied($coupon);

        $couponUsageRepository = Mockery::mock(CouponUsageRepository::class);
        $couponUsageRepository->shouldReceive('getCouponUsagesForClient')->andReturn([]);
        $couponUsageRepository->shouldReceive('getGlobalCouponUsages')->andReturn([[], [], []]);

        $discountCalculator = new CouponDiscountCalculator($cartServiceFactoryMock, new CouponValidator(
            $couponUsageRepository,
            Mockery::mock(ClientService::class)->shouldIgnoreMissing(),
        ));

        $discountCalculator->calculateDiscount($coupon, Mockery::mock(Cart::class));
    }

    private function givenWeHaveACartWithATotalOf(int $cartAmount): \Mockery\LegacyMockInterface
    {
        $cartServiceFactoryMock = Mockery::mock(CartServiceFactory::class);
        $cartServiceMock = Mockery::mock(CartService::class);
        $cartServiceMock->shouldReceive('getCartTotal')->andReturn(new Price($cartAmount));
        $cartServiceMock->shouldReceive('hasDiscountedProducts')->andReturnFalse();
        $cartServiceMock->shouldReceive('getCartTotalWithoutTaxesBeforeDiscounts')->andReturn(new Price($cartAmount));
        $cartServiceFactoryMock->shouldReceive('getCartService')->andReturn($cartServiceMock);
        return $cartServiceFactoryMock;
    }

    private function givenWeHaveACartWithDiscountedProducts(): \Mockery\LegacyMockInterface
    {
        $cartServiceFactoryMock = Mockery::mock(CartServiceFactory::class);
        $cartServiceMock = Mockery::mock(CartService::class);
        $cartServiceMock->shouldReceive('hasDiscountedProducts')->andReturnTrue();
        $cartServiceMock->shouldReceive('getCartTotal')->andReturn(new Price(100));
        $cartServiceFactoryMock->shouldReceive('getCartService')->andReturn($cartServiceMock);
        $cartServiceMock->shouldReceive('getCartTotalWithoutTaxesBeforeDiscounts')->andReturn(new Price(100));
        return $cartServiceFactoryMock;
    }

    private function givenWeHaveACartWithACouponApplied(Coupon $coupon): \Mockery\LegacyMockInterface
    {
        $cartServiceFactoryMock = Mockery::mock(CartServiceFactory::class);
        $cartServiceMock = Mockery::mock(CartService::class);
        $cartServiceMock->shouldReceive('getCartTotal')->andReturn(new Price(100));
        $cartServiceMock->shouldReceive('hasDiscountedProducts')->andReturnFalse();
        $cartServiceFactoryMock->shouldReceive('getCartService')->andReturn($cartServiceMock);
        $cartServiceMock->shouldReceive('getCartTotalWithoutTaxesBeforeDiscounts')->andReturn(new Price(100));
        return $cartServiceFactoryMock;
    }

    /**
     * @return CouponValidator
     */
    public function getCouponValidator(): CouponValidator
    {
        $couponUsageRepository = Mockery::mock(CouponUsageRepository::class);
        $couponUsageRepository->shouldIgnoreMissing();
        $couponUsageRepository->shouldReceive('getGlobalCouponUsages')->andReturn([])->byDefault();
        $couponUsageRepository->shouldReceive('getCouponUsagesForClient')->andReturn([])->byDefault();
        return new CouponValidator($couponUsageRepository, Mockery::mock(ClientService::class));
    }
}
