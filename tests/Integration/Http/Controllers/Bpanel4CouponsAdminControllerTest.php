<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Coupons\Tests\Integration\Http\Controllers;

use Bittacora\Bpanel4\Coupons\Database\Factories\CouponFactory;
use Bittacora\Bpanel4\Coupons\Http\Controllers\Bpanel4CouponsAdminController;
use Bittacora\Bpanel4\Coupons\Models\Coupon;
use Bittacora\Bpanel4Users\Tests\Helpers\AdminHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;

#[CoversClass(Bpanel4CouponsAdminController::class)]
final class Bpanel4CouponsAdminControllerTest extends TestCase
{
    use RefreshDatabase;

    #[DataProvider('protectedRouteDataProvider')]
    public function testLasRutasNoSonAccesiblesSinLosPermisos(string $routeName, bool $useCoupon): void
    {
        (new AdminHelper())->actingAsAdmin();
        $coupon = (new CouponFactory())->createOne();
        $this->get(route($routeName, $useCoupon ? ['coupon' => $coupon] : []))->assertForbidden();
    }

    /**
     * @return array{0:string, 1:bool}
     */
    public static function protectedRouteDataProvider(): array
    {
        return [
            ['bpanel4-coupons.index', false],
            ['bpanel4-coupons.create', false],
            ['bpanel4-coupons.edit', true],
        ];
    }
}